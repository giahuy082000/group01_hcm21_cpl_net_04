import React from 'react'
import * as Icon from '@material-ui/icons'
export const SidebarData=[
    {
        title:"Trang chủ",
        icon:<Icon.Home/> ,
        link:"/teacher"
    },
    {
        title:"Diễn đàn",
        icon:<Icon.Group/> ,
        link:"/forum"
    },
    {
        title:"Thông tin cá nhân",
        icon:<Icon.Person/> ,
        link:"/teacher/profileteacher"
    },
    {
        title:"Đổi mật khẩu",
        icon:<Icon.Settings/> ,
        link:"/doimatkhau"
    },
]