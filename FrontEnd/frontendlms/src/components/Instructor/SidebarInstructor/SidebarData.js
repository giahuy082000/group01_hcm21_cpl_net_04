import React from 'react'
import * as Icon from '@material-ui/icons'
export const SidebarData=[
    {
        title:"Trang chủ",
        icon:<Icon.Home/> ,
        link:"/instructor"
    },
    {
        title:"Diễn đàn",
        icon:<Icon.Group/> ,
        link:"/forum"
    },
    {
        title:"Thông tin cá nhân",
        icon:<Icon.Person/> ,
        link:"/instructor/profileinstructor"
    },
    {
        title:"Đổi mật khẩu",
        icon:<Icon.Settings/> ,
        link:"/doimatkhau"
    },
]