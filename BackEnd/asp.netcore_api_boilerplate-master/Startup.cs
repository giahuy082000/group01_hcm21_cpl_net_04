using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Extensions;
using Core.Entities.Identity;
using Core.Interfaces;
using Infrastructure.Data;
using Infrastructure.Identity;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace lms
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddDbContext<AppIdentityDbContext>(options =>
      {
        // Đọc chuỗi kết nối
        string connectstring = Configuration.GetConnectionString("WebApiDatabase");
        // Sử dụng MS SQL Server
        options.UseSqlServer(connectstring);
      });

      services.AddIdentity<AppUser, IdentityRole>()
      .AddEntityFrameworkStores<AppIdentityDbContext>()
      .AddDefaultTokenProviders();
      services.AddScoped<IUploadService, UploadService>();
      services.AddScoped<ICourseRepository, CourseRepository>();
      services.AddScoped<ICreateAssigmentsRepository, CreateAssigmentsRepository>();
      services.AddScoped<IRoadMapsRepository, RoadMapsRepository>();
      services.AddScoped<IForumsRepository, ForumsRepository>();
      services.AddScoped<IRoadmapContentRepository, RoadMapContentRepository>();
      services.AddScoped<ICommentRepository, CommentRepository>();
      services.AddScoped<IDocumentsRepository, DocumentsRepository>();
      services.AddScoped<IEveluateCourseRepository, EveluateCoursesRepository>();
      services.AddScoped<IStudentScoresRepository, StudentScoresRepository>();
      services.AddScoped<IStudentCoursesRepository, StudentCoursesRepository>();
      services.AddScoped<IMentorCoursesRepository, MentorCoursesRepository>();


      // services.con
      services.AddControllers();


      services.AddSwaggerGen(c =>
      {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "lms", Version = "v1" });
      });
      services.AddCors(opt =>
      {
        opt.AddPolicy("CorsPolicyHttp", policy =>
               {
                 policy.AllowAnyHeader().AllowAnyMethod().WithOrigins("http://localhost:3000");
               });
        opt.AddPolicy("CorsPolicyHttps", policy =>
        {
          policy.AllowAnyHeader().AllowAnyMethod().WithOrigins("https://localhost:5001");
        });
      });
      services.AddControllersWithViews()
            .AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

      services.AddApplicationServices();
      services.AddIdentityServices(Configuration);
      services.AddAutoMapper(typeof(Startup));
            services.AddControllersWithViews();

      services.Configure<IdentityOptions>(options =>
      {

        // Thiết lập về Password
        options.Password.RequiredLength = 6; // Số ký tự tối thiểu của password

        // Cấu hình Lockout - khóa user
        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5); // Khóa 5 phút
        options.Lockout.MaxFailedAccessAttempts = 5; // Thất bại 5 lầ thì khóa
        options.Lockout.AllowedForNewUsers = true;

        // Cấu hình về User.
        options.User.AllowedUserNameCharacters = // các ký tự đặt tên user
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
        options.User.RequireUniqueEmail = true;  // Email là duy nhất

        // Cấu hình đăng nhập.
        options.SignIn.RequireConfirmedEmail = true;            // Cấu hình xác thực địa chỉ email (email phải tồn tại)
        options.SignIn.RequireConfirmedPhoneNumber = true;     // Xác thực số điện thoại

      });

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseSwagger();
        app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "lms v1"));
      }

      app.UseHttpsRedirection();

      app.UseRouting();

      // Thêm CORS cho Swagger http và https
      app.UseCors("CorsPolicyHttp");
      app.UseCors("CorsPolicyHttps");

      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
  }
}
