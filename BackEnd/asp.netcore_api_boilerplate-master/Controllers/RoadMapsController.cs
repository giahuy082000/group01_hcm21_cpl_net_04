using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Core.Entities.Identity;
using API.Extensions;

namespace identity.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoadMapsController : ControllerBase
    {
        private readonly IRoadMapsRepository _roadMapsRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly UserManager<AppUser> _userManager;
        private readonly IStudentCoursesRepository _studentCourseRepository;
        public RoadMapsController(IRoadMapsRepository roadMapsRepository, ICourseRepository courseRepository, UserManager<AppUser> userManager, IStudentCoursesRepository studentCoursesRepository)
        {
            _roadMapsRepository = roadMapsRepository;
            _courseRepository = courseRepository;
            _userManager = userManager;
            _studentCourseRepository = studentCoursesRepository;
        }
        
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getRoadMap/{id}")]
        public async Task<ActionResult<RoadMaps>> getRoadMap(string id)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var RoadMapResult = await _roadMapsRepository.FindById(id);

                if (RoadMapResult != null)
                {
                    var result = new
                    {
                        Message = "Get RoadMap Success",
                        data = RoadMapResult,
                        Success = true,
                    };
                    return Ok(result);
                }
                return BadRequest(new ApiResponse(400, "Get RoadMap Failed"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getAllRoadMapsOfCourse/{idCourse}")]
        public async Task<ActionResult<List<RoadMaps>>> GetAllRoadMapsOfCourse(string idCourse)
        {
            try
            {
                var RoadMaps = await _roadMapsRepository.GetAllRoadMapsOfCourse(idCourse);
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleUser = await _userManager.GetRolesAsync(user);
                var courses = await _courseRepository.FindById(idCourse);
                var studentCourses = await _studentCourseRepository.FindByIdCourseAndIdUser(idCourse, user.Id);
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher)
                || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor)
                || (roleUser[0] == "CLASSADMIN" && user.Id == courses.idClassAdmin)
                || (roleUser[0] == "STUDENT" && studentCourses != null))
                {
                    var result = new
                    {
                        Message = "Get All RoadMaps Of Course Success",
                        data = RoadMaps,
                        Success = true,
                    };
                    return Ok(result);

                }
                else
                {
                    return BadRequest(new ApiResponse(400, "Bạn không có quyền truy cập!"));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("searchRoadMaps/{searchRoadMaps}")]
        public async Task<ActionResult<List<RoadMaps>>> FindSearchRoadMaps([FromQuery] string idCourse, string searchRoadMaps, [FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            try
            {

                if (pageSize < 8 || pageSize > 20)
                {
                    pageSize = 8;
                }
                if (pageNumber < 1)
                {
                    pageNumber = 1;
                }
                var RoadMaps = await _roadMapsRepository.FindByTitle(searchRoadMaps, pageSize, pageNumber);
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleUser = await _userManager.GetRolesAsync(user);
                var courses = await _courseRepository.FindById(idCourse);
                var studentCourses = await _studentCourseRepository.FindByIdCourseAndIdUser(idCourse, user.Id);
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher)
                || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor)
                || (roleUser[0] == "CLASSADMIN" && user.Id == courses.idClassAdmin)
                || (roleUser[0] == "STUDENT" && studentCourses != null))
                {
                    var result = new
                    {
                        Message = "Get RoadMaps Success",
                        data = RoadMaps,
                        Success = true,
                    };
                    if (RoadMaps == null)
                    {
                        return BadRequest(new ApiResponse(404, "No data found!"));
                    }
                    return Ok(result);
                }
                else
                {
                    return BadRequest(new ApiResponse(400, "Bạn không có quyền truy cập!"));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("createRoadMaps")]
        public async Task<ActionResult<RoadMaps>> CreateRoadMaps(RoadMapsDto roadMapsDto)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleUser = await _userManager.GetRolesAsync(user);
                var courses = await _courseRepository.FindById(roadMapsDto.idCourse);
                if (courses == null)
                    return BadRequest(new ApiResponse(404, "Do Not Course"));
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher) || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor))
                {
                    Guid id = Guid.NewGuid();
                    var roadMaps = new RoadMaps()
                    {
                        Id = id.ToString(),
                        idCourse = roadMapsDto.idCourse,
                        title = roadMapsDto.Title,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                    };
                    var roadMapsResult = await _roadMapsRepository.CreateRoadMaps(roadMaps);
                    if (roadMapsResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Create RoadMaps Success",
                            data = roadMaps,
                            Success = true,
                        };
                        return Ok(result);
                    }
                }
                return BadRequest(new ApiResponse(400, "Create Failed RoadMaps"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("updateRoadMaps/{id}")]
        public async Task<ActionResult<RoadMaps>> UpdateRoadMaps(string id, RoadMapsDto roadMapsDto)
        {
            try
            {
                var roadMapsCurrent = await _roadMapsRepository.FindById(id);
                if (roadMapsCurrent == null)
                {
                    return BadRequest(new ApiResponse(404, "RoadMap doesn't exist"));
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var courses = await _courseRepository.FindById(roadMapsCurrent.idCourse);
                var roleUser = await _userManager.GetRolesAsync(user);
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher) || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor))
                {
                    roadMapsCurrent.idCourse = roadMapsDto.idCourse;
                    roadMapsCurrent.title = roadMapsDto.Title;
                    roadMapsCurrent.updatedAt = DateTime.Now;
                    var roadMapsResult = await _roadMapsRepository.UpdateRoadMaps(roadMapsCurrent);

                    if (roadMapsResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Update RoadMap Success",
                            data = roadMapsCurrent,
                            Success = true,
                        };
                        return Ok(result);
                    }
                    else if (roadMapsResult == "RoadMap doesn't exist")
                    {
                        return BadRequest(new ApiResponse(404, "RoadMap doesn't exist"));
                    }
                }
                return BadRequest(new ApiResponse(400, "Update Failed RoadMap"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }


        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpDelete("deleteRoadMaps/{id}")]
        public async Task<ActionResult<RoadMaps>> DeleteRoadMaps(string id)
        {

            try
            {
                var roadMapsCurrent = await _roadMapsRepository.FindById(id);
                if (roadMapsCurrent == null)
                {
                    return BadRequest(new ApiResponse(404, "RoadMap doesn't exist"));
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var courses = await _courseRepository.FindById(roadMapsCurrent.idCourse);
                var roleUser = await _userManager.GetRolesAsync(user);
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher) || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor))
                {
                    roadMapsCurrent.updatedAt = DateTime.Now;
                    Console.WriteLine(roadMapsCurrent);
                    var roadMapsResult = await _roadMapsRepository.DeleteRoadMaps(id);
                    if (roadMapsResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Delete RoadMaps Success",
                            Success = true,
                        };
                        return Ok(result);
                    }
                    else if (roadMapsResult == "RoadMap doesn't exist")
                    {
                        return BadRequest(new ApiResponse(404, "RoadMap doesn't exist"));
                    }

                }
                return BadRequest(new ApiResponse(400, "Delete Failed RoadMaps"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }
    }
}
