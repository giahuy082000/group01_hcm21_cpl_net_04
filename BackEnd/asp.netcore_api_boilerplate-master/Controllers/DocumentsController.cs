using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Extensions;
using Core.Entities;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace identity.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentsController : ControllerBase
    {
        private readonly IDocumentsRepository _documents;
        private readonly UserManager<AppUser> _userManager;
        private readonly IRoadmapContentRepository _roadmapContent;
        private readonly IUploadService _uploadService;
        private readonly ICourseRepository _courseRepository;
        private readonly IRoadMapsRepository _roadMapsRepository;

        public DocumentsController(IDocumentsRepository DocumentsRepository, UserManager<AppUser> userManager, IRoadmapContentRepository RoadMapContentsRepository, IUploadService uploadService, ICourseRepository courseRepository, IRoadMapsRepository roadMapsRepository)
        {
            _documents = DocumentsRepository;
            _userManager = userManager;
            _roadmapContent = RoadMapContentsRepository;
            _roadMapsRepository = roadMapsRepository;
            _uploadService = uploadService;
            _courseRepository = courseRepository;
        }

        [HttpGet("getDocument/{id}")]
        public async Task<ActionResult<Documents>> getDocument(string id)
        {
            try
            {
                var DocumentResult = await _documents.FindById(id);
                if (DocumentResult != null)
                {
                    var result = new
                    {
                        Message = "Get Document Success",
                        data = DocumentResult,
                        Success = true,
                    };
                    return Ok(result);
                }
                return BadRequest(new ApiResponse(400, "Get Document Failed"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getLinkDocument/{id}")]
        public async Task<ActionResult<String>> GetLinkDocument(string id)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var documentsCurrent = await _documents.FindById(id);
                if (documentsCurrent == null)
                {
                    return BadRequest(new ApiResponse(400, "Find Failed Documents"));
                }
                var uploadService = _uploadService.GetLinkDocument(documentsCurrent.urlDocument);
                if (uploadService != null)
                {
                    var result = new
                    {
                        Message = "Get Link Success",
                        data = uploadService,
                        Success = true,
                    };
                    return Ok(result);
                }
                return BadRequest(new ApiResponse(400, "Get Failed Document"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }
        [HttpGet("searchDocuments/{searchDocuments}")]
        public async Task<ActionResult<List<Documents>>> FindSearch(string searchDocuments, [FromQuery] int pageNumber,[FromQuery] int pageSize)
        {
            if (pageSize < 8 || pageSize > 20)
            {
                pageSize = 8;
            }
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }
            var Documents = await _documents.FindByTitle(searchDocuments, pageSize, pageNumber);
            var result = new
            {
                Message = "Get Documents Success",
                data = Documents,
                Success = true,
            };
            if (Documents == null)
            {
                return BadRequest(new ApiResponse(404, "No data found!"));
            }
            return Ok(result);
        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("createDocuments")]
        public async Task<ActionResult<Documents>> CreateDocuments([FromForm] DocumentsDto documentsDto)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleUser = await _userManager.GetRolesAsync(user);
                
                var checkRoadmapContent = await _roadmapContent.FindById(documentsDto.idRoadmapcontent);
                if (checkRoadmapContent == null)
                    return BadRequest(new ApiResponse(404, "Do Not RoadMapContent"));
                var roadmaps = await _roadMapsRepository.FindById(checkRoadmapContent.idRoadMap);
                var courses = await _courseRepository.FindById(roadmaps.idCourse);
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher)|| (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor))
                {
                    Guid id = Guid.NewGuid();
                    var resultUpload = await _uploadService.UploadDocument(id.ToString(), documentsDto.URLDocument);
                    var document = new Documents()
                    {
                        Id = id.ToString(),
                        roadmapContentId = documentsDto.idRoadmapcontent,
                        idCreator = user.Id,
                        title = documentsDto.Title,
                        content = documentsDto.Content,
                        urlDocument = resultUpload.nameFile,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                    };
                    var DocumentsResult = await _documents.CreateDocuments(document);
                    var resultData = new
                    {
                        Id = id.ToString(),
                        roadmapContentId = documentsDto.idRoadmapcontent,
                        idCreator = user.Id,
                        title = documentsDto.Title,
                        content = documentsDto.Content,
                        urlDocument = resultUpload.nameFile,
                        linkDocument = resultUpload.url,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                    };
                    if (DocumentsResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Create Documents Success",
                            data = resultData,
                            Success = true,
                        };
                        return Ok(result);
                    }
                }
                return BadRequest(new ApiResponse(400, "Create Failed Documents"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("updateDocuments/{id}")]
        public async Task<ActionResult<Documents>> UpdateDocuments(string id, [FromForm] DocumentsDto documentsDto)
        {

            try
            {
                var documentsCurrent = await _documents.FindById(id);
                if (documentsCurrent == null)
                {
                    return BadRequest(new ApiResponse(404, "Document doesn't exist"));
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                if (user.Id == documentsCurrent.idCreator)
                {
                    if (documentsDto.URLDocument != null)
                    {
                        var resultUpload = await _uploadService.UploadDocument(documentsCurrent.Id, documentsDto.URLDocument);
                        if (resultUpload == null)
                        {
                            return BadRequest(new ApiResponse(400, "Update Failed Document"));
                        }
                        documentsCurrent.roadmapContentId = documentsDto.idRoadmapcontent;
                        documentsCurrent.idCreator = user.Id;
                        documentsCurrent.content = documentsDto.Content;
                        documentsCurrent.title = documentsDto.Title;
                        documentsCurrent.urlDocument = resultUpload.nameFile;
                        documentsCurrent.updatedAt = DateTime.Now;
                        var DocumentsResult = await _documents.UpdateDocuments(documentsCurrent);
                        var resultData = new
                        {
                            Id = id.ToString(),
                            roadmapContentId = documentsDto.idRoadmapcontent,
                            idCreator = user.Id,
                            title = documentsDto.Title,
                            content = documentsDto.Content,
                            urlDocument = resultUpload.nameFile,
                            linkDocument = resultUpload.url,
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now,
                        };
                        if (DocumentsResult == "Success")
                        {
                            var result = new
                            {
                                Message = "Update Document Success",
                                data = resultData,
                                Success = true,
                            };
                            return Ok(result);
                        }
                        else if (DocumentsResult == "Document doesn't exist")
                        {
                            return BadRequest(new ApiResponse(404, "Document doesn't exist"));
                        }
                    }
                    else
                    {
                        documentsCurrent.roadmapContentId = documentsDto.idRoadmapcontent;
                        documentsCurrent.idCreator = user.Id;
                        documentsCurrent.content = documentsDto.Content;
                        documentsCurrent.title = documentsDto.Title;
                        documentsCurrent.urlDocument = documentsCurrent.urlDocument;
                        documentsCurrent.updatedAt = DateTime.Now;
                        var documentsResult = await _documents.UpdateDocuments(documentsCurrent);
                        if (documentsResult == "Success")
                        {
                            var result = new
                            {
                                Message = "Update Document Success",
                                data = documentsCurrent,
                                Success = true,
                            };
                            return Ok(result);
                        }
                        else if (documentsResult == "Document doesn't exist")
                        {
                            return BadRequest(new ApiResponse(404, "Document doesn't exist"));
                        }
                    }
                }
                return BadRequest(new ApiResponse(400, "Update Failed Document"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpDelete("deleteDocuments/{id}")]
        public async Task<ActionResult<Documents>> DeleteDocuments(string id)
        {
            try
            {
                var documentsCurrent = await _documents.FindById(id);
                if (documentsCurrent == null)
                {
                    return BadRequest(new ApiResponse(404, "Document doesn't exist"));
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                if (user.Id == documentsCurrent.idCreator)
                {
                    var documentsResult = await _documents.DeleteDocuments(id);
                    if (documentsResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Delete Document Success",
                            data = documentsCurrent,
                            Success = true,
                        };
                        return Ok(result);
                    }
                    else if (documentsResult == "Document doesn't exist")
                    {
                        return BadRequest(new ApiResponse(404, "Document doesn't exist"));
                    }
                }
                return BadRequest(new ApiResponse(400, "Delete Document faild"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

    }
}
