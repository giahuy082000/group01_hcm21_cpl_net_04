using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using API.Extensions;
using Core.Entities;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace identity.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MentorCoursesController : ControllerBase
    {
        private readonly IMentorCoursesRepository _mentorCourseRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly IStudentCoursesRepository _studentCourseRepository;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public MentorCoursesController(IMentorCoursesRepository mentorCoursesRepository, UserManager<AppUser> userManager, ICourseRepository courseRepository, RoleManager<IdentityRole> roleManager, IStudentCoursesRepository studentCoursesRepository)
        {
            _courseRepository = courseRepository;
            _studentCourseRepository = studentCoursesRepository;
            _mentorCourseRepository = mentorCoursesRepository;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpGet("getMentorCourse/{id}")]
        public async Task<ActionResult<MentorCourses>> getMentorCourse(string id)
        {
            try
            {
                var mentorCourseResult = await _mentorCourseRepository.FindById(id);
                if (mentorCourseResult != null)
                {
                    var result = new
                    {
                        Message = "Get MentorCourse Success",
                        data = mentorCourseResult,
                        Success = true,
                    };
                    return Ok(result);
                }
                return BadRequest(new ApiResponse(400, "Get MentorCourse Failed"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [HttpPost("SendEmail")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public void SendEmail(string email, string subject, string content)
        {
            string fromMail = "lmsneithgroup01@gmail.com";
            string fromPassword = "NhutThien123";

            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromMail);
            message.Subject = subject;
            message.To.Add(new MailAddress(email));
            message.Body = content;
            message.IsBodyHtml = true;

            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential(fromMail, fromPassword),
                EnableSsl = true,
            };
            smtpClient.Send(message);
        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getListMentorCourses/{status}")]
        public async Task<ActionResult<List<MentorCourses>>> GetListMentorCourses(string status, [FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            if (pageSize < 8 || pageSize > 20)
            {
                pageSize = 8;
            }
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }
            var MentorCourses = await _mentorCourseRepository.GetListMentorCourseAsync(status, pageSize, pageNumber);
            var result = new
            {
                Message = "Get List MentorCourses Success",
                data = MentorCourses,
                Success = true,
            };
            return Ok(result);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getMentorCoursesOfCourses/{idCourses}")]
        public async Task<ActionResult<List<MentorCourses>>> GetListMentorCoursesOfCourses(string idCourse, [FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            if (pageSize < 8 || pageSize > 20)
            {
                pageSize = 8;
            }
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }
            var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
            var roleUser = await _userManager.GetRolesAsync(user);
            var courses = await _courseRepository.FindById(idCourse);
            var studentCourses = await _studentCourseRepository.FindByIdCourseAndIdUser(idCourse, user.Id);
            if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher)
            || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor)
            || (roleUser[0] == "CLASSADMIN" && user.Id == courses.idClassAdmin)
            || (roleUser[0] == "STUDENT" && studentCourses != null))
            {
                var MentorCourses = await _mentorCourseRepository.FindByIdCourse(idCourse, pageSize, pageNumber);
                var result = new
                {
                    Message = "Get List MentorCourses Of Courses Success",
                    data = MentorCourses,
                    Success = true,
                };
                return Ok(result);

            }
            else
            {
                return BadRequest(new ApiResponse(400, "Bạn không có quyền truy cập!"));
            }

        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("createMentorCourses")]
        public async Task<ActionResult<MentorCourses>> CreateMentorCourses(MentorCoursesDto mentorCoursesDto)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var courses = await _courseRepository.FindById(mentorCoursesDto.idCourse);
                if (courses == null)
                    return BadRequest(new ApiResponse(404, "Do Not Course"));
                var roleUser = await _userManager.GetRolesAsync(user);
                Console.WriteLine(roleUser[0]);
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher) || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor))
                {
                    var checkMentorCourse = await _mentorCourseRepository.FindByIdCourseAndIdUser(mentorCoursesDto.idCourse, mentorCoursesDto.idStudent);
                    if (checkMentorCourse != null)
                        return BadRequest(new ApiResponse(400, "Học viên đã là MENTOR hoặc đang chờ phê duyệt"));
                    Guid id = Guid.NewGuid();
                    var MentorCourses = new MentorCourses()
                    {
                        Id = id.ToString(),
                        idCourse = mentorCoursesDto.idCourse,
                        idUser = mentorCoursesDto.idStudent,
                        status = "AWAIT",
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                    };
                    var mentorCoursesResult = await _mentorCourseRepository.CreateMentorCourse(MentorCourses);
                    if (mentorCoursesResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Create MentorCourses Success",
                            data = MentorCourses,
                            Success = true,
                        };
                        return Ok(result);
                    }
                }
                return BadRequest(new ApiResponse(400, "Create Failed MentorCourses"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("activateMentorCourses")]
        public async Task<ActionResult<MentorCourses>> ActivateMentorCourse(MentorCoursesDto mentorCoursesDto)
        {
            try
            {

                var mentorCoursesCurrent = await _mentorCourseRepository.FindByIdCourseAndIdUser(mentorCoursesDto.idCourse, mentorCoursesDto.idStudent);
                Console.WriteLine(mentorCoursesCurrent);
                if (mentorCoursesCurrent == null)
                {
                    return BadRequest(new ApiResponse(404, "MentorCourses doesn't exist"));
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var coursesCurrent = await _courseRepository.FindById(mentorCoursesDto.idCourse);
                var mentor = await _userManager.FindByIdAsync(mentorCoursesDto.idStudent);
                var classAdmin = await _userManager.FindByIdAsync(user.Id);
                var roleClassAdmin = await _userManager.GetRolesAsync(user);
                if (roleClassAdmin[0] == "CLASSADMIN" && user.Id == coursesCurrent.idClassAdmin)
                {
                    mentorCoursesCurrent.idCourse = mentorCoursesDto.idCourse;
                    mentorCoursesCurrent.idUser = mentorCoursesDto.idStudent;
                    mentorCoursesCurrent.status = "ACTIVE";
                    mentorCoursesCurrent.updatedAt = DateTime.Now;
                    var mentorCoursesResult = await _mentorCourseRepository.UpdateMentorCourse(mentorCoursesCurrent);
                    string content = "Bạn đã được xác nhận trở thành MENTOR của khóa học: " + coursesCurrent.name;
                    string subject = "Phê Duyệt MENTOR";
                    SendEmail(mentor.Email, subject, content);

                    if (mentorCoursesResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Update MentorCourses Success",
                            data = mentorCoursesCurrent,
                            Success = true,
                        };
                        return Ok(result);
                    }
                    else if (mentorCoursesResult == "MentorCourses doesn't exist")
                    {
                        return BadRequest(new ApiResponse(404, "MentorCourses doesn't exist"));
                    }
                }

                return BadRequest(new ApiResponse(400, "Update Failed MentorCourses"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("cancelMentorCourses")]
        public async Task<ActionResult<MentorCourses>> CancelMentorCourse(MentorCoursesDto mentorCoursesDto)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var coursesCurrent = await _courseRepository.FindById(mentorCoursesDto.idCourse);
                if (coursesCurrent == null)
                {
                    return BadRequest(new ApiResponse(404, "Course doesn't exist"));
                }

                if (mentorCoursesDto.idStudent == null)
                {
                    var mentorCoursesCurrent = await _mentorCourseRepository.FindByIdCourseAndIdUser(mentorCoursesDto.idCourse, user.Id);
                    Console.WriteLine(mentorCoursesCurrent);
                    if (mentorCoursesCurrent == null)
                    {
                        return BadRequest(new ApiResponse(404, "MentorCourse doesn't exist"));
                    }
                    var roleUser = await _userManager.GetRolesAsync(user);
                    if (roleUser[0] == "STUDENT" && user.Id == mentorCoursesCurrent.idUser)
                    {
                        mentorCoursesCurrent.idCourse = mentorCoursesDto.idCourse;
                        mentorCoursesCurrent.idUser = user.Id;
                        mentorCoursesCurrent.status = "CANCEL";
                        mentorCoursesCurrent.updatedAt = DateTime.Now;
                        var mentorCoursesResult = await _mentorCourseRepository.UpdateMentorCourse(mentorCoursesCurrent);
                        string content = "Bạn đã từ chối trở thành MENTOR của khóa học: " + coursesCurrent.name;
                        SendEmail(user.Email, "Từ Chối MENTOR", content);

                        if (mentorCoursesResult == "Success")
                        {
                            var result = new
                            {
                                Message = "Update MentorCourses Success",
                                data = mentorCoursesCurrent,
                                Success = true,
                            };
                            return Ok(result);
                        }
                        else if (mentorCoursesResult == "MentorCourses doesn't exist")
                        {
                            return BadRequest(new ApiResponse(404, "MentorCourses doesn't exist"));
                        }
                    }
                }
                else
                {
                    var mentorCoursesCurrent = await _mentorCourseRepository.FindByIdCourseAndIdUser(mentorCoursesDto.idCourse, mentorCoursesDto.idStudent);
                    Console.WriteLine(mentorCoursesCurrent);
                    if (mentorCoursesCurrent == null)
                    {
                        return BadRequest(new ApiResponse(404, "MentorCourse doesn't exist"));
                    }
                    var mentor = await _userManager.FindByIdAsync(mentorCoursesDto.idStudent);
                    var classAdmin = await _userManager.FindByIdAsync(user.Id);
                    var roleClassAdmin = await _userManager.GetRolesAsync(user);
                    if (roleClassAdmin[0] == "CLASSADMIN" && user.Id == coursesCurrent.idClassAdmin)
                    {
                        mentorCoursesCurrent.idCourse = mentorCoursesDto.idCourse;
                        mentorCoursesCurrent.idUser = mentorCoursesDto.idStudent;
                        mentorCoursesCurrent.status = "CANCEL";
                        mentorCoursesCurrent.updatedAt = DateTime.Now;
                        var mentorCoursesResult = await _mentorCourseRepository.UpdateMentorCourse(mentorCoursesCurrent);

                        string content = "Bạn không được phê duyệt trở thành MENTOR của khóa học: " + coursesCurrent.name;
                        string subject = "Phê Duyệt MENTOR";

                        SendEmail(mentor.Email, subject, content);

                        if (mentorCoursesResult == "Success")
                        {
                            var result = new
                            {
                                Message = "Update MentorCourses Success",
                                data = mentorCoursesCurrent,
                                Success = true,
                            };
                            return Ok(result);
                        }
                        else if (mentorCoursesResult == "MentorCourses doesn't exist")
                        {
                            return BadRequest(new ApiResponse(404, "MentorCourses doesn't exist"));
                        }
                    }

                }
                return BadRequest(new ApiResponse(400, "Update Failed MentorCourses"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

    }
}
