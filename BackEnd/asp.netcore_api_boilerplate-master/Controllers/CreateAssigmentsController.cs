using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Extensions;
using Core.Entities;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace identity.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CreateAssigmentsController : ControllerBase
  {
    private readonly ICreateAssigmentsRepository _createAssigments;
    private readonly IRoadmapContentRepository _roadmapContent;
    private readonly UserManager<AppUser> _userManager;
    private readonly IUploadService _uploadService;


    public CreateAssigmentsController(ICreateAssigmentsRepository createAssigments, IRoadmapContentRepository roadmapContent, UserManager<AppUser> userManager, IUploadService uploadService)
    {
      _createAssigments = createAssigments;
      _roadmapContent = roadmapContent;
      _userManager = userManager;
      _uploadService = uploadService;
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("getLinkAssigments/{id}")]
    public async Task<ActionResult<String>> GetLinkAssigments(string id)
    {
      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var assigmentCurrent = await _createAssigments.FindById(id);
        if (assigmentCurrent == null)
        {
          return BadRequest(new ApiResponse(400, "Find Failed Assigments"));
        }
        var uploadService = _uploadService.GetLinkAssigment(assigmentCurrent.urlFile);
        if (uploadService != null)
        {
          var result = new
          {
            Message = "Get Link Success",
            data = uploadService,
            Success = true,
          };
          return Ok(result);
        }
        return BadRequest(new ApiResponse(400, "Create Failed Assigments"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("getAssigments/{id}")]
    public async Task<ActionResult<CreateAssignments>> GetAssigments(string id)
    {
      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var AssigmentsResult = await _createAssigments.GetAllAssignmentsByRoadMapContent(id);

        if (AssigmentsResult != null)
        {
          var result = new
          {
            Message = "Create Assigments Success",
            data = AssigmentsResult,
            Success = true,
          };
          return Ok(result);
        }
        return BadRequest(new ApiResponse(400, "Create Failed Assigments"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("getAssigmentsByCourse")]
    public async Task<ActionResult<CreateAssignments>> GetAssigmentsByCourse([FromQuery] string idCourse, [FromQuery] int pageNumber, [FromQuery] int pageSize)
    {
      try
      {
        if (pageSize < 8 || pageSize > 20)
        {
          pageSize = 8;
        }
        if (pageNumber < 1)
        {
          pageNumber = 1;
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roleUser = await _userManager.GetRolesAsync(user);
        if (roleUser[0] == "TEACHER" || roleUser[0] == "INSTRUCTOR")
        {
          var AssigmentsResult = await _createAssigments.GetAllAssignmentsByIDCourse(idCourse, pageNumber, pageSize);
          foreach (var item in AssigmentsResult)
          {
            var getlinkAssigment = _uploadService.GetLinkAssigment(item.urlFile);
            item.urlFile = getlinkAssigment.linkUrl;
          }
          if (AssigmentsResult != null)
          {
            var result = new
            {
              Message = "Create Assigments Success",
              data = AssigmentsResult,
              Success = true,
            };
            return Ok(result);
          }
        }

        return BadRequest(new ApiResponse(400, "Create Failed Assigments"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPost("createAssigments")]
    public async Task<ActionResult<CreateAssignments>> CreateAssigments([FromForm] CreateAssigmentsDto createAssigmentsDto)
    {
      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roleUser = await _userManager.GetRolesAsync(user);
        var checkRoadmapContent = await _roadmapContent.FindById(createAssigmentsDto.idRoadMapContent);
        if (checkRoadmapContent == null)
          return BadRequest(new ApiResponse(404, "Do Not RoadMapContent"));
        if (roleUser[0] == "TEACHER" || roleUser[0] == "INSTRUCTOR")
        {
          Guid id = Guid.NewGuid();
          var resultUpload = await _uploadService.UploadAssigment(id.ToString(), createAssigmentsDto.URLFile);
          var createAssigments = new CreateAssignments()
          {
            Id = id.ToString(),
            idCreator = user.Id,
            idRoadMapContent = createAssigmentsDto.idRoadMapContent,
            title = createAssigmentsDto.title,
            content = createAssigmentsDto.content,
            start = DateTime.Parse(createAssigmentsDto.start),
            finish = DateTime.Parse(createAssigmentsDto.finish),
            urlFile = resultUpload.nameFile,
            createdAt = DateTime.Now,
            updatedAt = DateTime.Now,
          };
          var AssigmentsResult = await _createAssigments.CreateAssigments(createAssigments);
          var resultData = new
          {
            Id = id.ToString(),
            idCreator = user.Id,
            idRoadMapContent = createAssigmentsDto.idRoadMapContent,
            title = createAssigmentsDto.title,
            content = createAssigmentsDto.content,
            start = DateTime.Parse(createAssigmentsDto.start),
            finish = DateTime.Parse(createAssigmentsDto.finish),
            urlFile = resultUpload.nameFile,
            linkAssigments = resultUpload.url,
            createdAt = DateTime.Now,
            updatedAt = DateTime.Now,
          };
          if (AssigmentsResult == "Success")
          {
            var result = new
            {
              Message = "Create Assigments Success",
              data = resultData,
              Success = true,
            };
            return Ok(result);
          }
        }
        return BadRequest(new ApiResponse(400, "Create Failed Assigments"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPut("updateAssigments/{id}")]
    public async Task<ActionResult<CreateAssignments>> UpdateAssigments(string id, [FromForm] CreateAssigmentsDto createAssigmentsDto)
    {
      try
      {
        var assigmentsCurrent = await _createAssigments.FindById(id);
        if (assigmentsCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "Assigments doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        if (user.Id == assigmentsCurrent.idCreator)
        {
          if (createAssigmentsDto.URLFile != null)
          {
            var resultUpload = await _uploadService.UploadAssigment(assigmentsCurrent.Id, createAssigmentsDto.URLFile);
            if (resultUpload == null)
            {
              return BadRequest(new ApiResponse(400, "Update Failed Assigment"));
            }
            assigmentsCurrent.title = createAssigmentsDto.title;
            assigmentsCurrent.content = createAssigmentsDto.content;
            assigmentsCurrent.start = DateTime.Parse(createAssigmentsDto.start);
            assigmentsCurrent.finish = DateTime.Parse(createAssigmentsDto.finish);
            assigmentsCurrent.urlFile = resultUpload.nameFile;
            assigmentsCurrent.createdAt = assigmentsCurrent.createdAt;
            assigmentsCurrent.updatedAt = DateTime.Now;
            var assigmentsResult = await _createAssigments.UpdateAssigments(assigmentsCurrent);
            var resultData = new
            {
              Id = id.ToString(),
              idCreator = user.Id,
              idRoadMapContent = createAssigmentsDto.idRoadMapContent,
              title = createAssigmentsDto.title,
              content = createAssigmentsDto.content,
              start = DateTime.Parse(createAssigmentsDto.start),
              finish = DateTime.Parse(createAssigmentsDto.finish),
              urlFile = resultUpload.nameFile,
              linkAssigments = resultUpload.url,
              createdAt = DateTime.Now,
              updatedAt = DateTime.Now,
            };
            if (assigmentsResult == "Success")
            {
              var result = new
              {
                Message = "Update Assigment Success",
                data = resultData,
                Success = true,
              };
              return Ok(result);
            }
            else if (assigmentsResult == "Assigment doesn't exist")
            {
              return BadRequest(new ApiResponse(404, "Assigment doesn't exist"));
            }
          }
          else
          {
            assigmentsCurrent.title = createAssigmentsDto.title;
            assigmentsCurrent.content = createAssigmentsDto.content;
            assigmentsCurrent.start = DateTime.Parse(createAssigmentsDto.start);
            assigmentsCurrent.finish = DateTime.Parse(createAssigmentsDto.finish);
            assigmentsCurrent.urlFile = assigmentsCurrent.urlFile;
            assigmentsCurrent.createdAt = assigmentsCurrent.createdAt;
            assigmentsCurrent.updatedAt = DateTime.Now;
            var assigmentsResult = await _createAssigments.UpdateAssigments(assigmentsCurrent);
            if (assigmentsResult == "Success")
            {
              var result = new
              {
                Message = "Update Assigment Success",
                data = assigmentsCurrent,
                Success = true,
              };
              return Ok(result);
            }
            else if (assigmentsResult == "Assigment doesn't exist")
            {
              return BadRequest(new ApiResponse(404, "Assigment doesn't exist"));
            }
          }
        }
        return BadRequest(new ApiResponse(400, "Update Failed Assigment"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpDelete("deleteAssigments/{id}")]
    public async Task<ActionResult<CreateAssignments>> deleteAssigments(string id)
    {
      try
      {
        var assigmentsCurrent = await _createAssigments.FindById(id);
        if (assigmentsCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "Assigments doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        if (user.Id == assigmentsCurrent.idCreator)
        {
          var assigmentsResult = await _createAssigments.DeleteAssigments(id);
          if (assigmentsResult == "Success")
          {
            var result = new
            {
              Message = "Delete Assigment Success",
              data = assigmentsCurrent,
              Success = true,
            };
            return Ok(result);
          }
          else if (assigmentsResult == "Assigment doesn't exist")
          {
            return BadRequest(new ApiResponse(404, "Assigment doesn't exist"));
          }
        }
        return BadRequest(new ApiResponse(400, "Delete Assigment faild"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("getDeadLineAssigment")]
    public async Task<ActionResult<CreateAssignments>> GetDeadLineAssigment([FromQuery] String timeStart, [FromQuery] String timeEnd)
    {
      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roleUser = await _userManager.GetRolesAsync(user);
        var timeMonthStart = DateTime.Parse(timeStart);
        Console.WriteLine(timeMonthStart);
        var timeMonthEnd = DateTime.Parse(timeEnd);
        timeMonthEnd = timeMonthEnd.AddSeconds(60 * 23 * 60 + 59 * 60 + 59);
        Console.WriteLine(timeMonthEnd);

        if (roleUser[0] == "STUDENT")
        {
          var AssigmentsResult = await _createAssigments.GetDeadLine(timeMonthStart, timeMonthEnd, user.Id);

          var result = new
          {
            Message = "Get Assigments DeadLine",
            data = AssigmentsResult,
            Success = true,
          };
          return Ok(result);
        }
        return BadRequest(new ApiResponse(400, "Get Failed Assigments DeadLine"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
  }
}



