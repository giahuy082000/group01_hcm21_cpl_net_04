using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Extensions;
using Core.Entities;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;


namespace identity.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ForumsController : ControllerBase
    {
        private readonly IForumsRepository _forumsRepository;
        private readonly UserManager<AppUser> _userManager;
        private readonly IUploadService _uploadService;
        public ForumsController(IForumsRepository ForumsRepository, UserManager<AppUser> userManager, IUploadService uploadService)
        {
            _forumsRepository = ForumsRepository;
            _userManager = userManager;
            _uploadService = uploadService;
        }


        [HttpGet("getForum/{id}")]
        public async Task<ActionResult<Forums>> getForum(string id)
        {
            try
            {
                var forumResult = await _forumsRepository.FindById(id);
                if (forumResult != null)
                {
                    var result = new
                    {
                        Message = "Get Forum Success",
                        data = forumResult,
                        Success = true,
                    };
                    return Ok(result);
                }
                return BadRequest(new ApiResponse(400, "Get Forum Failed"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [HttpGet("getAllForums")]
        public async Task<ActionResult<List<Forums>>> GetAllForums([FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            if (pageSize < 8 || pageSize > 20)
            {
                pageSize = 8;
            }
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }
            var Forums = await _forumsRepository.GetAllForumsAsync(pageSize, pageNumber);
            var result = new
            {
                Message = "Get All Forums Success",
                data = Forums,
                Success = true,
            };
            if (Forums == null)
            {
                return BadRequest(new ApiResponse(404, "No data found!"));
            }
            return Ok(result);
        }
        [HttpGet("searchForums/{searchForums}")]
        public async Task<ActionResult<List<Forums>>> FindSearch(string searchForums, [FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            if (pageSize < 8 || pageSize > 20)
            {
                pageSize = 8;
            }
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }
            var Forums = await _forumsRepository.FindByTitle(searchForums, pageSize, pageNumber);
            var result = new
            {
                Message = "Get Forums Success",
                data = Forums,
                Success = true,
            };
            return Ok(result);
        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("createForums")]
        public async Task<ActionResult<Forums>> CreateForums([FromForm] ForumsDto forumsDto)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleUser = await _userManager.GetRolesAsync(user);
                if (roleUser[0] == "INSTRUCTOR")
                {
                    Guid id = Guid.NewGuid();
                    var resultUpload = await _uploadService.UploadImage(id.ToString(), forumsDto.UrlImage);
                    var forums = new Forums()
                    {
                        Id = id.ToString(),
                        idUser = user.Id,
                        title = forumsDto.Title,
                        content = forumsDto.Content,
                        urlImage = resultUpload.nameFile,
                        status = "ACTIVE",
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                    };
                    var forumsResult = await _forumsRepository.CreateForums(forums);
                    var resultData = new
                    {
                        Id = id.ToString(),
                        idUser = user.Id,
                        title = forumsDto.Title,
                        content = forumsDto.Content,
                        urlImage = resultUpload.nameFile,
                        linkImage = resultUpload.url,
                        status = "ACTIVE",
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                    };
                    if (forumsResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Create Forums Success",
                            data = resultData,
                            Success = true,
                        };
                        return Ok(result);
                    }
                }
                return BadRequest(new ApiResponse(400, "Create Failed Forums"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }


        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("updateForums/{id}")]
        public async Task<ActionResult<Forums>> UpdateForums(string id, [FromForm] ForumsDto forumsDto)
        {
            try
            {
                var forumsCurrent = await _forumsRepository.FindById(id);
                if (forumsCurrent == null)
                {
                    return BadRequest(new ApiResponse(404, "Forum doesn't exist"));
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                if (user.Id == forumsCurrent.idUser)
                {
                    if (forumsDto.UrlImage != null)
                    {
                        var resultUpload = await _uploadService.UploadImage(forumsCurrent.Id, forumsDto.UrlImage);
                        if (resultUpload == null)
                        {
                            return BadRequest(new ApiResponse(400, "Update Failed Forum"));
                        }
                        forumsCurrent.idUser = user.Id;
                        forumsCurrent.content = forumsDto.Content;
                        forumsCurrent.title = forumsDto.Title;
                        forumsCurrent.urlImage = resultUpload.nameFile;
                        forumsCurrent.updatedAt = DateTime.Now;
                        var forumsResult = await _forumsRepository.UpdateForums(forumsCurrent);
                        var resultData = new
                        {
                            Id = id.ToString(),
                            idUser = user.Id,
                            content = forumsDto.Content,
                            title = forumsDto.Title,
                            urlImage = resultUpload.nameFile,
                            linkImage = resultUpload.url,
                            updatedAt = DateTime.Now,
                        };
                        if (forumsResult == "Success")
                        {
                            var result = new
                            {
                                Message = "Update Forum Success",
                                data = resultData,
                                Success = true,
                            };
                            return Ok(result);
                        }
                        else if (forumsResult == "Forum doesn't exist")
                        {
                            return BadRequest(new ApiResponse(404, "Forum doesn't exist"));
                        }
                    }
                    else
                    {
                        forumsCurrent.idUser = user.Id;
                        forumsCurrent.content = forumsDto.Content;
                        forumsCurrent.title = forumsDto.Title;
                        forumsCurrent.urlImage = forumsCurrent.urlImage;
                        forumsCurrent.updatedAt = DateTime.Now;
                        var forumsResult = await _forumsRepository.UpdateForums(forumsCurrent);
                        if (forumsResult == "Success")
                        {
                            var result = new
                            {
                                Message = "Update Forum Success",
                                data = forumsCurrent,
                                Success = true,
                            };
                            return Ok(result);
                        }
                        else if (forumsResult == "Forum doesn't exist")
                        {
                            return BadRequest(new ApiResponse(404, "Forum doesn't exist"));
                        }
                    }
                }
                return BadRequest(new ApiResponse(400, "Update Failed Forum"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpDelete("deleteForums/{id}")]
        public async Task<ActionResult<Forums>> DeleteForums(string id)
        {

            try
            {
                var forumCurrent = await _forumsRepository.FindById(id);
                if (forumCurrent == null)
                {
                    return BadRequest(new ApiResponse(404, "Forum doesn't exist"));
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleInstructor = await _userManager.GetRolesAsync(user);
                if (user.Id == forumCurrent.idUser && roleInstructor[0] == "INSTRUCTOR")
                {
                    forumCurrent.status = "DELETED";
                    forumCurrent.updatedAt = DateTime.Now;
                    var forumResult = await _forumsRepository.UpdateForums(forumCurrent);
                    if (forumResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Delete Forum Success",
                            Success = true,
                        };
                        return Ok(result);
                    }
                }
                return BadRequest(new ApiResponse(400, "Delete Failed Forum"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

    }
}
