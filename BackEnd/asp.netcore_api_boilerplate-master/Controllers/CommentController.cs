using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Extensions;
using Core.Entities;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace identity.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CommentController : ControllerBase
  {
    private readonly ICommentRepository _commentRepository;
    private readonly UserManager<AppUser> _userManager;
    public CommentController(ICommentRepository commentRepository, UserManager<AppUser> userManager)
    {
      _commentRepository = commentRepository;
      _userManager = userManager;
    }
    [HttpGet("getAllComment/{idForum}")]
    public async Task<ActionResult<List<Comments>>> GetAllComment(string idForum)
    {
      Console.WriteLine(idForum);
      var courses = await _commentRepository.GetAllCommentAsync(idForum);
      var result = new
      {
        Message = "Get All Comments Success",
        data = courses,
        Success = true,
      };
      return Ok(result);
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPost("createComment")]
    public async Task<ActionResult<Courses>> CreateComment(CommentDto commentDto)
    {
      try
      {
        Console.WriteLine("id forum", commentDto.idForum);
        Guid id = Guid.NewGuid();
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var comment = new Comments()
        {
          Id = id.ToString(),
          idForum = commentDto.idForum,
          idUser = user.Id,
          status = "ACTIVE",
          content = commentDto.content
        };
        var commentResult = await _commentRepository.CreateComment(comment);
        if (commentResult == "Success")
        {
          var result = new
          {
            Message = "Create Comment Success",
            data = comment,
            Success = true,
          };
          return Ok(result);
        }
        return BadRequest(new ApiResponse(400, "Create Comment Faild"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPut("updateComment/{id}")]
    public async Task<ActionResult<Comments>> UpdateComment(string id, Comments comments)
    {
      try
      {
        var commentCurrent = await _commentRepository.FindById(id);
        Console.WriteLine(commentCurrent.idUser.ToString());
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        if (commentCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "Comment doesn't exist"));
        }
        if (user.Id == commentCurrent.idUser)
        {
          commentCurrent.content = comments.content;
          commentCurrent.createdAt = comments.createdAt;
          commentCurrent.updatedAt = DateTime.Now;
          var commentResult = await _commentRepository.UpdateComment(commentCurrent);
          if (commentResult == "Success")
          {
            var result = new
            {
              Message = "Update Comment Success",
              data = commentCurrent,
              Success = true,
            };
            return Ok(result);
          }
          else if (commentResult == "Comment doesn't exist")
          {
            return BadRequest(new ApiResponse(404, "Comment doesn't exist"));
          }
          return BadRequest(new ApiResponse(400, "Update Failed Comment"));
        }
        else
        {
          return BadRequest(new ApiResponse(400, "Không phải là người comment"));
        }
      }
      catch
      {
        return BadRequest(new ApiResponse(400, "Xác nhận thất bại"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpDelete("deleteComment/{id}")]
    public async Task<ActionResult<Comments>> deleteComment(string id)
    {
      try
      {
        var commentCurrent = await _commentRepository.FindById(id);
        Console.WriteLine(commentCurrent.idUser.ToString());
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        if (user.Id == commentCurrent.idUser)
        {
          if (commentCurrent == null)
          {
            return BadRequest(new ApiResponse(404, "Comment doesn't exist"));
          }
          commentCurrent.status = "DELETED";
          commentCurrent.updatedAt = DateTime.Now;
          var commentResult = await _commentRepository.UpdateComment(commentCurrent);
          if (commentResult == "Success")
          {
            var result = new
            {
              Message = "Delete Comment Success",
              data = commentCurrent,
              Success = true,
            };
            return Ok(result);
          }
          else if (commentResult == "Comment doesn't exist")
          {
            return BadRequest(new ApiResponse(404, "Comment doesn't exist"));
          }
          return BadRequest(new ApiResponse(400, "Delete Failed Comment"));
        }
        else
        {
          return BadRequest(new ApiResponse(400, "Không phải là người comment"));
        }
      }
      catch
      {
        return BadRequest(new ApiResponse(400, "Xác nhận thất bại"));
      }
    }
  }
}
