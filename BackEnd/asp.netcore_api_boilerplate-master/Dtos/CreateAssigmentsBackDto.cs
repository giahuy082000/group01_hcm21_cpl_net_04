using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities;
using Core.Entities.Identity;
using Microsoft.AspNetCore.Http;

namespace identity.Dto
{
    public class CreateAssigmentsBackDto
    {
        public string Id {get; set;}
        public string idCreator { get; set; }
        [ForeignKey("idCreator")]
        public AppUser Teacher { set; get; }
        public string idRoadMapContent { get; set; }
        public string title { set; get; }
        public string content { set; get; }
        public DateTime start { set; get; }
        public DateTime finish { set; get; }
        public string urlFile{set;get;} 
        public bool outofdate{set;get;}         

    }
}
