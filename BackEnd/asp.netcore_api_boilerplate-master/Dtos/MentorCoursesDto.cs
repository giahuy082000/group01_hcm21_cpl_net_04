using System.ComponentModel.DataAnnotations;

namespace identity.Dto
{
    public class MentorCoursesDto
    {
        [StringLength(36)]
        public string idCourse { get; set; }
        [StringLength(36)]
        public string idStudent { get; set; }

    }
}