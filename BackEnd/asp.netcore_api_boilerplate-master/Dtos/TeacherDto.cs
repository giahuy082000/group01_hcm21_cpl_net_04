namespace Core.Dto
{
    public class TeacherDto
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
    }
}
