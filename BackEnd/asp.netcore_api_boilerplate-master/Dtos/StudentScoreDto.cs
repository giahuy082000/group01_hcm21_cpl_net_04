using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace identity.Dto
{
    public class StudentScoreDto
    {
        [StringLength(36)]
        public string idCourse { get; set; }
        [StringLength(36)]
        public string idAssignment { get; set; }
        public IFormFile UrlSubmit { get; set; }

    }
}