namespace Core.Dto
{
    public class DeadLineDto
    {
        public string idAssigment { get; set; }
        public string idCourse { get; set; }
        public string nameAssigment { get; set; }
        public string nameCourse { get; set; }
    }
}
