using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace identity.Dto
{
    public class DocumentsDto
    {
        [StringLength(36)]
        public string idRoadmapcontent { get; set; }
         [StringLength(50)]
        public string Title { get; set; }
        [Required]
        [StringLength(120)]
        public string Content { get; set; }
        public IFormFile URLDocument { get; set; }

    }
}
