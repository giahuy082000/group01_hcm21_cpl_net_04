using System.ComponentModel.DataAnnotations;
namespace identity.Dto
{
    public class CommentDto
    {
    [Required]
    public string content { get; set; }
    [Required]
    public string idForum { get; set; }

    }
}
