using System;
using System.ComponentModel.DataAnnotations;
namespace identity.Dto
{
    public class EveluateCourseDto
    {
        [StringLength(36)]
        public string idCourse { get; set; }
        public string comment { get; set; }
        public float point { get; set; }
    }
}
