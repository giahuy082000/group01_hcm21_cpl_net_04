using System;
using Core.Entities.Identity;

namespace Core.Dto
{
  public class BackCommentByForumDto
  {
    public string Id { get; set; }
    public string idForum { get; set; }
    public string idUser { get; set; }
    public AppUser User { set; get; }
    // public string idReply { get; set; }
    public string status { get; set; }
    public string content { get; set; }
    public string urlImage { get; set; }
    public DateTime createdAt { get; set; }
    public DateTime updatedAt { get; set; }
  }
}
