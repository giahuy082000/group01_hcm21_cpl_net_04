namespace Core.Dto
{
    public class UserBackDto
    {
        public string id { get; set; }

        public string UserName { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string avatar { get; set; }
        public string role { get; set; }
        public string lockoutEnd {get; set;}
    }
}
