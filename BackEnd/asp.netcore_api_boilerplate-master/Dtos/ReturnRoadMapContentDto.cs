using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace identity.Dto
{
    public class ReturnRoadMapContentDto
    {
        public string Id { get; set; }
        public string idRoadMap { get; set; }
        public string title { set; get; }
        public string content { get; set; }
        public IList<CreateAssigmentsBackDto> createAssignment { get; set; }
        public IList<Documents> documents { get; set; }


    }
}