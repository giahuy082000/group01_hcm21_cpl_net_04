using System;

namespace Core.Dto
{
    public class BackAssigmentAllDto
    {
        public string idCreator { get; set; }
        public string idRoadMapContent { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public DateTime start { get; set; }
        public DateTime finish { get; set; }
        public string urlFile { get; set; }
        public string Id { get; set; }

    }
}
