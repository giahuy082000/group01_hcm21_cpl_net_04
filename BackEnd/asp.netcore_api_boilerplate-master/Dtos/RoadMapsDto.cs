using System;
using System.ComponentModel.DataAnnotations;
namespace identity.Dto
{
    public class RoadMapsDto
    {
        [StringLength(36)]
        public string idCourse { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
    }
}