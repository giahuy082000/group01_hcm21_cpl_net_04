using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
    public interface IEveluateCourseRepository
    {
        Task<List<EveluateCourses>> GetAllEveluateCoursesAsync(string id);
        Task<string> CreateEveluate(EveluateCourses eveluateCourses);
        Task<string> UpdateEveluate(EveluateCourses eveluateCourses);
        Task<string> DeleteEveluate(string id);
        Task<EveluateCourses> FindById(string id);
        Task<string> FindByUserAndCourseAsync(string idUser, string idCourse);
        // Task<List<EveluateCourses>> FindByTitle(string searchdocument);
  }
}