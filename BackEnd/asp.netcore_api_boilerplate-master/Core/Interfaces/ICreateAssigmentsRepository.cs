using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dto;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
    public interface ICreateAssigmentsRepository
    {
        Task<List<CreateAssignments>> GetAllAssignmentsByRoadMapContent(string id);
        Task<List<BackAssigmentAllDto>> GetAllAssignmentsByIDCourse(string id, int pageNumber, int pageSize);

        Task<string> CreateAssigments(CreateAssignments createAssignments );
        Task<string> UpdateAssigments(CreateAssignments createAssignments );
        Task<string> DeleteAssigments(string id);
        Task<CreateAssignments> FindById(string id);
        Task<List<DeadLineDto>> GetDeadLine(DateTime startTime, DateTime endTime, String idUser );
        
        // Task<List<Courses>> FindByName(string searchCourse);
  }
}