using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
    public interface IDocumentsRepository
    {
        Task<List<Documents>> GetAllDocumentsByRoadMapContent(string idRoadMapContent, int pageSize, int pageNumber);
        Task<string> CreateDocuments(Documents document);
        Task<string> UpdateDocuments(Documents document);
        Task<string> DeleteDocuments(string id);
        Task<Documents> FindById(string id);
        Task<List<Documents>> FindByTitle(string searchdocument, int pageSize, int pageNumber);
  }
}