using System.Threading.Tasks;
using Core.Dto;
using Core.Entities.Identity;
using identity.Dto;
using Microsoft.AspNetCore.Http;

namespace Core.Interfaces
{
    public interface IUploadService
    {
        Task<UploadDto> UploadAvatar(string id,IFormFile file);
        Task<UploadDto> UploadAssigment(string id, IFormFile file);
        Task<UploadDto> UploadDocument(string id,IFormFile file);
        Task<UploadDto> UploadSubmit(string idStudent, string idAssignment,IFormFile file);
        Task<UploadDto> UploadImage(string id,IFormFile file);
        Task<UploadDto> UploadImageCourse(string id,IFormFile file);

        GetLinkDto GetLinkAvatar(string name);
        GetLinkDto GetLinkAssigment(string name);
        GetLinkDto GetLinkDocument(string name);
        GetLinkDto GetLinkImage(string name);

    }
}