using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
    public interface IRoadmapContentRepository
    {
        Task<List<RoadMapContents>>  GetAllRoadMapContentsOfRM(string idRoadMaps, int pageSize, int pageNumber);
        Task<string> CreateRoadMapContent(RoadMapContents roadMapContents );
        Task<string> UpdateRoadMapContent(RoadMapContents roadMapContents );
        Task<string> DeleteRoadMapContent(string id);
        Task<RoadMapContents> FindById(string id);
        
  }
}