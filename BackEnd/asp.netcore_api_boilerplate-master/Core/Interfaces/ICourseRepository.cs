using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
    public interface ICourseRepository
    {
        Task<List<Courses>> GetAllCourseAsync(int pageSize, int pageNumber);
        Task<string> CreateCourse(Courses course );
        Task<string> UpdateCourse(Courses course );
        Task<Courses> FindById(string id);
        Task<List<Courses>> FindByName(string searchCourse,int pageSize, int pageNumber);
        Task<List<Courses>> FindCourseHistoryInstructor(string status, string idInstructor, string search, int pageSize, int pageNumber);
        Task<List<Courses>> FindCourseHistoryTeacher(string status, string idTeacher, string search, int pageSize, int pageNumber);
        Task<List<Courses>> FindCourseHistoryClassAdmin(string status, string idClassAdmin, string search, int pageSize, int pageNumber);

  }
}