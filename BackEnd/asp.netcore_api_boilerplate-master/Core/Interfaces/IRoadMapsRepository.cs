using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
    public interface IRoadMapsRepository
    {
        Task<List<RoadMaps>> GetAllRoadMapsOfCourse(string idCourse);

        Task<string> CreateRoadMaps(RoadMaps roadmap);
        Task<string> UpdateRoadMaps(RoadMaps roadmap);
        Task<string> DeleteRoadMaps(string id);
        Task<RoadMaps> FindById(string id);
        Task<List<RoadMaps>> FindByTitle(string searchroadmaps, int pageSize, int pageNumber);
  }
}