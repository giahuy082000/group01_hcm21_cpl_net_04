using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
    public interface IMentorCoursesRepository
    {
        Task<List<MentorCourses>> GetListMentorCourseAsync(string status, int pageSize, int pageNumber);
        Task<string> CreateMentorCourse(MentorCourses mentorCourse);
        Task<string> UpdateMentorCourse(MentorCourses mentorCourse);
        Task<MentorCourses> FindById(string idMentorCourse);
        Task<List<MentorCourses>> FindByIdCourse(string idCourse, int pageSize, int pageNumber);
        Task<MentorCourses> FindByIdCourseAndIdUser(string idCourse, string idUser);

    }
}