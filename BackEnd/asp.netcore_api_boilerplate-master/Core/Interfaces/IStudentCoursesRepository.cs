using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
  public interface IStudentCoursesRepository
  {
    Task<int> TotalOfStudents(string status, string idCourse);
    Task<List<StudentCourses>> GetListStudentCourseAsync(string idCourse, string status, int pageSize, int pageNumber);
    Task<string> CreateStudentCourse(StudentCourses studentCourse);
    Task<string> UpdateStudentCourse(StudentCourses studentCourse);
    Task<string> DeleteStudentCourse(string idCourse, string idUser);
    Task<StudentCourses> FindById(string idCourse, string idUser);
    Task<List<StudentCourses>> FindByIdCourse(string idCourse);
    Task<StudentCourses> FindByIdCourseAndIdUser(string idCourse, string idUser);

    Task<List<StudentCourses>> GetListHistoryCourseAsync(string status, string idStudent, int pageSize, int pageNumber);

    Task<List<StudentCourses>> FindCourseHistory(string status, string idStudent, string search, int pageSize, int pageNumber);

  }
}