using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.Identity;

namespace Core.Entities
{
    public class CreateAssignments : BaseEntity
    {

        public string idCreator { get; set; }
        [ForeignKey("idCreator")]
        public AppUser Teacher { set; get; }
        public string idRoadMapContent { get; set; }
        [ForeignKey("idRoadMapContent")]
        public RoadMapContents RoadMapContent { set; get; }
        public string title { set; get; }
        public string content { set; get; }
        public DateTime start { set; get; }
        public DateTime finish { set; get; }
        public string urlFile{set;get;}

    }
}