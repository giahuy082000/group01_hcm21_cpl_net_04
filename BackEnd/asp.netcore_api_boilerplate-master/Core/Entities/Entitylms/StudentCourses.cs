// entity "StudentCourse"  {
//     *courseId : string
//     *userId : string
//     --
//     *createAt : datetime
//     *updateAt : datetime
// }
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.Identity;

namespace Core.Entities
{
    public class StudentCourses : BaseTime
    {
        [Key]
        public string idCourse { get; set; }
        [ForeignKey("idCourse")]
        public Courses Course { set; get; }
        [Key]
        public string idUser { get; set; }
        [ForeignKey("idUser")]
        public AppUser Student { set; get; }

        public string status { get; set; }

    }
}