using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.Identity;

namespace Core.Entities
{
    public class RoadMapContents : BaseEntity
    {
        public string idRoadMap { get; set; }
        [ForeignKey("idRoadMap")]
        public RoadMaps RoadMap { set; get; }
        public string title {set;get;}
        public string content { get; set; }
        public IList<CreateAssignments> createAssignment { get; set; }
        public IList<Documents> documents { get; set; }

    }
}