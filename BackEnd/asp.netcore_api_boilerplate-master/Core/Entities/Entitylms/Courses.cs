using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.Identity;

namespace Core.Entities
{
    public class Courses : BaseEntity
    {
        public string idInstructor { get; set; }
        [ForeignKey("idInstructor")]
        public AppUser Instructor { set; get; }


        public string idTeacher { get; set; }
        [ForeignKey("idTeacher")]
        public AppUser Teacher { set; get; }


        public string idClassAdmin { get; set; }
        [ForeignKey("idClassAdmin")]
        public AppUser ClassAdmin { set; get; }
        
        public string name { get; set; }
        public string description { get; set; }

        public string status {get;set;}
        public int duration { get; set; }
        public string URLImage {get;set;}
    }
}