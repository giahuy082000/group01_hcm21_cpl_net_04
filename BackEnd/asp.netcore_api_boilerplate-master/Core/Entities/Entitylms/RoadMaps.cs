
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.Identity;

namespace Core.Entities
{
    public class RoadMaps : BaseEntity
    {
        public string idCourse { get; set; }
        [ForeignKey("idCourse")]
        public Courses Course { set; get; }

        public string title { get; set; }
        public IList<RoadMapContents> RoadMapContent { get; set; }

    }
}