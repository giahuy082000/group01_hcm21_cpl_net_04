using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.Identity;

namespace Core.Entities
{
    public class StudentScores : BaseTime
    {

        public string idCourse { get; set; }
        [ForeignKey("idCourse")]
        public Courses Courses { set; get; }
        public string idUser { set; get; }
        [ForeignKey("idUser")]
        public AppUser Student { set; get; }
        public string idAssignment { set; get; }
        [ForeignKey("idAssignment")]    
        public CreateAssignments CreateAssignment { set; get; }
        public string idPointer { set; get; }
        [ForeignKey("idPointer")]
        public AppUser User { set; get; }
        public float score{set;get;}

        public string urlAssignment { set; get; }

    }
}