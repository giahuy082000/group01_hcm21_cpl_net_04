using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.Identity;

namespace Core.Entities
{
    public class Comments : BaseEntity
    {

        public string idForum { get; set; }
        [ForeignKey("idForum")]
        public Forums Forum { set; get; }
        public string idUser { get; set; }
        [ForeignKey("idUser")]
        public AppUser User { set; get; }
        // public string idReply { get; set; }
        public string status { get; set; }
        public string content { get; set; }
    }
}