using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.Identity;

namespace Core.Entities
{
    public class Forums : BaseEntity
    {

        public string idUser { get; set; }
        [ForeignKey("idUser")]
        public AppUser Instructor { set; get; }
        public string title { get; set; }
        public string content { get; set; }
        public string urlImage { get; set; }
        public string status {get;set;}



    }
}