﻿// <auto-generated />
using System;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace lms.Migrations
{
    [DbContext(typeof(AppIdentityDbContext))]
    [Migration("20210905104205_v1")]
    partial class v1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.8")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Core.Entities.Comments", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("content")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("idForum")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("idUser")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("status")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("idForum");

                    b.HasIndex("idUser");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("Core.Entities.Courses", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("URLImage")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("duration")
                        .HasColumnType("int");

                    b.Property<string>("idClassAdmin")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("idInstructor")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("idTeacher")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("status")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("idClassAdmin");

                    b.HasIndex("idInstructor");

                    b.HasIndex("idTeacher");

                    b.ToTable("Courses");
                });

            modelBuilder.Entity("Core.Entities.CreateAssignments", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("content")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("finish")
                        .HasColumnType("datetime2");

                    b.Property<string>("idCreator")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("idRoadMapContent")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("start")
                        .HasColumnType("datetime2");

                    b.Property<string>("title")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("urlFile")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("idCreator");

                    b.HasIndex("idRoadMapContent");

                    b.ToTable("CreateAssignments");
                });

            modelBuilder.Entity("Core.Entities.Documents", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("content")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("idCreator")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("roadmapContentId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("title")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("urlDocument")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("idCreator");

                    b.HasIndex("roadmapContentId");

                    b.ToTable("Documents");
                });

            modelBuilder.Entity("Core.Entities.EveluateCourses", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("comment")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("idCourse")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("idUser")
                        .HasColumnType("nvarchar(450)");

                    b.Property<float>("point")
                        .HasColumnType("real");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("idCourse");

                    b.HasIndex("idUser");

                    b.ToTable("EveluateCourses");
                });

            modelBuilder.Entity("Core.Entities.Forums", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("content")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("idUser")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("status")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("title")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("urlImage")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("idUser");

                    b.ToTable("Forums");
                });

            modelBuilder.Entity("Core.Entities.Identity.AppUser", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("DisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("urlAvatar")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasDatabaseName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasDatabaseName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Core.Entities.MentorCourses", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("idCourse")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("idUser")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("status")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("idCourse");

                    b.HasIndex("idUser");

                    b.ToTable("MentorCourses");
                });

            modelBuilder.Entity("Core.Entities.RoadMapContents", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("content")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("idRoadMap")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("title")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("idRoadMap");

                    b.ToTable("RoadMapContents");
                });

            modelBuilder.Entity("Core.Entities.RoadMaps", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("idCourse")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("title")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("idCourse");

                    b.ToTable("RoadMaps");
                });

            modelBuilder.Entity("Core.Entities.StudentCourses", b =>
                {
                    b.Property<string>("idCourse")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("idUser")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("status")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("idCourse", "idUser");

                    b.HasIndex("idUser");

                    b.HasIndex("idCourse", "idUser")
                        .IsUnique();

                    b.ToTable("StudentCourses");
                });

            modelBuilder.Entity("Core.Entities.StudentScores", b =>
                {
                    b.Property<string>("idCourse")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("idUser")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("idAssignment")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("createdAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("idPointer")
                        .HasColumnType("nvarchar(450)");

                    b.Property<float>("score")
                        .HasColumnType("real");

                    b.Property<DateTime>("updatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("urlAssignment")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("idCourse", "idUser", "idAssignment");

                    b.HasIndex("idAssignment");

                    b.HasIndex("idPointer");

                    b.HasIndex("idUser");

                    b.HasIndex("idCourse", "idUser", "idAssignment")
                        .IsUnique();

                    b.ToTable("StudentScores");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasDatabaseName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("RoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("UserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("UserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("RoleId")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("UserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("UserTokens");
                });

            modelBuilder.Entity("Core.Entities.Comments", b =>
                {
                    b.HasOne("Core.Entities.Forums", "Forum")
                        .WithMany()
                        .HasForeignKey("idForum");

                    b.HasOne("Core.Entities.Identity.AppUser", "User")
                        .WithMany()
                        .HasForeignKey("idUser");

                    b.Navigation("Forum");

                    b.Navigation("User");
                });

            modelBuilder.Entity("Core.Entities.Courses", b =>
                {
                    b.HasOne("Core.Entities.Identity.AppUser", "ClassAdmin")
                        .WithMany()
                        .HasForeignKey("idClassAdmin");

                    b.HasOne("Core.Entities.Identity.AppUser", "Instructor")
                        .WithMany()
                        .HasForeignKey("idInstructor");

                    b.HasOne("Core.Entities.Identity.AppUser", "Teacher")
                        .WithMany()
                        .HasForeignKey("idTeacher");

                    b.Navigation("ClassAdmin");

                    b.Navigation("Instructor");

                    b.Navigation("Teacher");
                });

            modelBuilder.Entity("Core.Entities.CreateAssignments", b =>
                {
                    b.HasOne("Core.Entities.Identity.AppUser", "Teacher")
                        .WithMany()
                        .HasForeignKey("idCreator");

                    b.HasOne("Core.Entities.RoadMapContents", "RoadMapContent")
                        .WithMany("createAssignment")
                        .HasForeignKey("idRoadMapContent")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.Navigation("RoadMapContent");

                    b.Navigation("Teacher");
                });

            modelBuilder.Entity("Core.Entities.Documents", b =>
                {
                    b.HasOne("Core.Entities.Identity.AppUser", "Teacher")
                        .WithMany()
                        .HasForeignKey("idCreator");

                    b.HasOne("Core.Entities.RoadMapContents", "RoadMapContent")
                        .WithMany("documents")
                        .HasForeignKey("roadmapContentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.Navigation("RoadMapContent");

                    b.Navigation("Teacher");
                });

            modelBuilder.Entity("Core.Entities.EveluateCourses", b =>
                {
                    b.HasOne("Core.Entities.Courses", "Course")
                        .WithMany()
                        .HasForeignKey("idCourse");

                    b.HasOne("Core.Entities.Identity.AppUser", "User")
                        .WithMany()
                        .HasForeignKey("idUser");

                    b.Navigation("Course");

                    b.Navigation("User");
                });

            modelBuilder.Entity("Core.Entities.Forums", b =>
                {
                    b.HasOne("Core.Entities.Identity.AppUser", "Instructor")
                        .WithMany()
                        .HasForeignKey("idUser");

                    b.Navigation("Instructor");
                });

            modelBuilder.Entity("Core.Entities.MentorCourses", b =>
                {
                    b.HasOne("Core.Entities.Courses", "Course")
                        .WithMany()
                        .HasForeignKey("idCourse");

                    b.HasOne("Core.Entities.Identity.AppUser", "Mentor")
                        .WithMany()
                        .HasForeignKey("idUser");

                    b.Navigation("Course");

                    b.Navigation("Mentor");
                });

            modelBuilder.Entity("Core.Entities.RoadMapContents", b =>
                {
                    b.HasOne("Core.Entities.RoadMaps", "RoadMap")
                        .WithMany("RoadMapContent")
                        .HasForeignKey("idRoadMap")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.Navigation("RoadMap");
                });

            modelBuilder.Entity("Core.Entities.RoadMaps", b =>
                {
                    b.HasOne("Core.Entities.Courses", "Course")
                        .WithMany()
                        .HasForeignKey("idCourse");

                    b.Navigation("Course");
                });

            modelBuilder.Entity("Core.Entities.StudentCourses", b =>
                {
                    b.HasOne("Core.Entities.Courses", "Course")
                        .WithMany()
                        .HasForeignKey("idCourse")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Core.Entities.Identity.AppUser", "Student")
                        .WithMany()
                        .HasForeignKey("idUser")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Course");

                    b.Navigation("Student");
                });

            modelBuilder.Entity("Core.Entities.StudentScores", b =>
                {
                    b.HasOne("Core.Entities.CreateAssignments", "CreateAssignment")
                        .WithMany()
                        .HasForeignKey("idAssignment")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Core.Entities.Courses", "Courses")
                        .WithMany()
                        .HasForeignKey("idCourse")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Core.Entities.Identity.AppUser", "User")
                        .WithMany()
                        .HasForeignKey("idPointer");

                    b.HasOne("Core.Entities.Identity.AppUser", "Student")
                        .WithMany()
                        .HasForeignKey("idUser")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Courses");

                    b.Navigation("CreateAssignment");

                    b.Navigation("Student");

                    b.Navigation("User");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Core.Entities.Identity.AppUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Core.Entities.Identity.AppUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Core.Entities.Identity.AppUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("Core.Entities.Identity.AppUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Core.Entities.RoadMapContents", b =>
                {
                    b.Navigation("createAssignment");

                    b.Navigation("documents");
                });

            modelBuilder.Entity("Core.Entities.RoadMaps", b =>
                {
                    b.Navigation("RoadMapContent");
                });
#pragma warning restore 612, 618
        }
    }
}
