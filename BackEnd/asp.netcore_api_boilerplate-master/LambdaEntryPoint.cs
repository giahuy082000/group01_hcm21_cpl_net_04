using Amazon.Lambda.AspNetCoreServer;
using lms;
using Microsoft.AspNetCore.Hosting;

namespace lms
{
    public class LambdaEntryPoint : APIGatewayHttpApiV2ProxyFunction
    {
        protected override void Init(IWebHostBuilder builder)
        {
            builder.UseStartup<Startup>();
        }
    }
}