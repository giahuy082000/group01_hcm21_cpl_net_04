using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
  public class MentorCoursesRepository : IMentorCoursesRepository
  {
    private readonly AppIdentityDbContext _context;
    public MentorCoursesRepository(AppIdentityDbContext context)
    {
      _context = context;
    }
    public async Task<string> CreateMentorCourse(MentorCourses MentorCourse)
    {
      try
      {
        _context.MentorCourses.Add(MentorCourse);
        var check = await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    public async Task<string> UpdateMentorCourse(MentorCourses mentorCourse)
    {
      _context.Entry(mentorCourse).State = EntityState.Modified;
      if (!MentorCoursesExists(mentorCourse.Id))
      {
        return "MentorCourse doesn't exist";
      }
      if (!MentorCoursesExistsCourse(mentorCourse.idCourse))
      {
        return "Course doesn't exist";
      }
      else if (!MentorCoursesExistsUser(mentorCourse.idUser))
      {
        return "User doesn't exist";
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }

    public async Task<List<MentorCourses>> GetListMentorCourseAsync(string status, int pageSize, int pageNumber)
    {
      if (status == "ALL")
      {
        return await _context.MentorCourses.ToListAsync();
      }
      return await _context.MentorCourses.Where(e => e.status == status).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }

    public async Task<MentorCourses> FindById(string idMentorCourse)
    {
      if (MentorCoursesExists(idMentorCourse) == true)
      {
        var MentorCourse = await _context.MentorCourses.FindAsync(idMentorCourse);
        return MentorCourse;
      }
      else
      {
        return null;
      }
    }
    private bool MentorCoursesExists(string idMentorCourse)
    {
      return _context.MentorCourses.Any(e => e.Id == idMentorCourse);
    }
    private bool MentorCoursesExistsCourse(string idCourse)
    {
      return _context.MentorCourses.Any(e => e.idCourse == idCourse);
    }
    private bool MentorCoursesExistsUser(string idUser)
    {
      return _context.MentorCourses.Any(e => e.idUser == idUser);
    }

    public async Task<List<MentorCourses>> FindByIdCourse(string idCourse, int pageSize, int pageNumber)
    {
      var status = "ACTIVE";
      return await _context.MentorCourses.Where(e => e.idCourse == idCourse && e.status == status).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }
    public async Task<MentorCourses> FindByIdCourseAndIdUser(string idCourse, string idUser)
    {
      if (MentorCoursesExistsCourse(idCourse) == true && MentorCoursesExistsUser(idUser) == true)
      {
        var mentorCourse = await _context.MentorCourses.Where(e => e.idCourse == idCourse && e.idUser == idUser).FirstOrDefaultAsync();
        return mentorCourse;
      }
      else
      {
        return null;
      }
    }

   
  }
}