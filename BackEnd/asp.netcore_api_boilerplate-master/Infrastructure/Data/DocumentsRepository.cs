using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
  public class DocumentsRepository : IDocumentsRepository
  {
    private readonly AppIdentityDbContext _context;
    public DocumentsRepository(AppIdentityDbContext context)
    {
      _context = context;
    }

    public async Task<string> CreateDocuments(Documents Documents)
    {
      try
      {
        _context.Documents.Add(Documents);
        var check = await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    public async Task<string> UpdateDocuments(Documents Documents)
    {
      _context.Entry(Documents).State = EntityState.Modified;
      if (!DocumentsExists(Documents.Id))
      {
        return "Documents doesn't exist";
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }

    public async Task<List<Documents>> GetAllDocumentsByRoadMapContent(string idRoadMapContent, int pageSize, int pageNumber)
    {
      return await _context.Documents.Where(x => x.roadmapContentId == idRoadMapContent).OrderBy(x=> x.createdAt).ToListAsync();
    }
    public async Task<Documents> FindById(string id)
    {
      if (DocumentsExists(id) == true)
      {
        var Documents = await _context.Documents.FindAsync(id);
        return Documents;
      }
      else
      {
        return null;
      }
    }

     public async Task<string> DeleteDocuments(string id)
        {
            var Documents = await _context.Documents.FindAsync(id);

            if (!DocumentsExists(Documents.Id))
            {
                return "Documents doesn't exist";
            }
            try
            {
                _context.Documents.Remove(Documents);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return "Failed";
            }
            return "Success";
        }
    private bool DocumentsExists(string id)
    {
      return _context.Documents.Any(e => e.Id == id);
    }

    public async Task<List<Documents>> FindByTitle(string searchDocuments, int pageSize, int pageNumber)
    {
      return await _context.Documents.Where(c => c.title.Contains(searchDocuments)).ToListAsync();
    }
  }
}