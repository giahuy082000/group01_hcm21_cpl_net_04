using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
  public class CourseRepository : ICourseRepository
  {
    private readonly AppIdentityDbContext _context;
    public CourseRepository(AppIdentityDbContext context)
    {
      _context = context;
    }

    public async Task<string> CreateCourse(Courses course)
    {
      try
      {
        _context.Courses.Add(course);
        var check = await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    public async Task<string> UpdateCourse(Courses course)
    {
      _context.Entry(course).State = EntityState.Modified;
      if (!CourseExists(course.Id))
      {
        return "course doesn't exist";
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }

    public async Task<List<Courses>> GetAllCourseAsync(int pageNumber, int pageSize)
    {
      var status = "ACTIVE";
      return await _context.Courses.Where(e => e.status == status).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }
    public async Task<Courses> FindById(string id)
    {
      if (CourseExists(id) == true)
      {
        var course = await _context.Courses.FindAsync(id);
        return course;
      }
      else
      {
        return null;
      }
    }
    private bool CourseExists(string id)
    {
      return _context.Courses.Any(e => e.Id == id);
    }

    public async Task<List<Courses>> FindByName(string searchCourse, int pageNumber, int pageSize)
    {
      var status = "ACTIVE";
      return await _context.Courses.Where(c => c.name.Contains(searchCourse) && c.status == status).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }
    public async Task<List<Courses>> FindCourseHistoryInstructor(string status, string idInstructor, string search, int pageSize, int pageNumber)
    {
      if (status == "ALL")
      {
        return await _context.Courses.Where(e => e.idInstructor == idInstructor && e.name.Contains(search)).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
      }
      return await _context.Courses.Where(e => e.status == status && e.idInstructor == idInstructor && e.name.Contains(search)).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }
    public async Task<List<Courses>> FindCourseHistoryTeacher(string status, string idTeacher, string search, int pageSize, int pageNumber)
    {
      if (status == "ALL")
      {
        return await _context.Courses.Where(e => e.idTeacher == idTeacher && e.name.Contains(search)).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
      }
      return await _context.Courses.Where(e => e.status == status && e.idTeacher == idTeacher && e.name.Contains(search)).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }
    public async Task<List<Courses>> FindCourseHistoryClassAdmin(string status, string idClassAdmin, string search, int pageSize, int pageNumber)
    {
      if (status == "ALL")
      {
        return await _context.Courses.Where(e => e.idClassAdmin == idClassAdmin && e.name.Contains(search)).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
      }
      return await _context.Courses.Where(e => e.idClassAdmin == status && e.idTeacher == idClassAdmin && e.name.Contains(search)).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }

  }
}