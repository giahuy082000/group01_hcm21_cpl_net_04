using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
  public class StudentCoursesRepository : IStudentCoursesRepository
  {
    private readonly AppIdentityDbContext _context;
    public StudentCoursesRepository(AppIdentityDbContext context)
    {
      _context = context;
    }

    public async Task<int> TotalOfStudents(string status, string idCourse)
    {
      var listStudent = await _context.StudentCourses.Where(e => e.status == status && e.idCourse == idCourse).ToListAsync();
      if (idCourse == "ALL")
      {
        listStudent = await _context.StudentCourses.Where(e => e.status == status).ToListAsync();
      }
      return listStudent.Count();
    }

    public async Task<string> CreateStudentCourse(StudentCourses StudentCourse)
    {
      try
      {
        _context.StudentCourses.Add(StudentCourse);
        var check = await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    public async Task<string> UpdateStudentCourse(StudentCourses studentCourse)
    {
      _context.Entry(studentCourse).State = EntityState.Modified;
      if (!StudentCoursesExistsCourse(studentCourse.idCourse))
      {
        return "Course doesn't exist";
      }
      else if (!StudentCoursesExistsUser(studentCourse.idUser))
      {
        return "User doesn't exist";
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }

    
    public async Task<string> DeleteStudentCourse(string idCourse, string idUser)
    {
      var studentCourse = await _context.StudentCourses.FindAsync(idCourse, idUser);
      
       if (!StudentCoursesExistsCourse(idCourse))
      {
        return "Course doesn't exist";
      }
      else if (!StudentCoursesExistsUser(idUser))
      {
        return "User doesn't exist";
      }
      try
      {
        _context.StudentCourses.Remove(studentCourse);
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }


    public async Task<List<StudentCourses>> GetListStudentCourseAsync(string idCourse, string status, int pageSize, int pageNumber)
    {
      if (status == "ALL")
      {
        return await _context.StudentCourses.Where(e => e.idCourse == idCourse).ToListAsync();
      }
      return await _context.StudentCourses.Where(e => e.idCourse == idCourse && e.status == status).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }

    public async Task<StudentCourses> FindById(string idCourse, string idUser)
    {
      if (StudentCoursesExistsCourse(idCourse) == true && StudentCoursesExistsUser(idUser) == true)
      {
        var StudentCourse = await _context.StudentCourses.FindAsync(idCourse, idUser);
        return StudentCourse;
      }
      else
      {
        return null;
      }
    }
    private bool StudentCoursesExistsCourse(string idCourse)
    {
      return _context.StudentCourses.Any(e => e.idCourse == idCourse);
    }
    private bool StudentCoursesExistsUser(string idUser)
    {
      return _context.StudentCourses.Any(e => e.idUser == idUser);
    }

    public async Task<List<StudentCourses>> FindByIdCourse(string idCourse)
    {
      var status = "STUDYING";
      return await _context.StudentCourses.Where(e => e.idCourse == idCourse && e.status == status).ToListAsync();
    }

    public async Task<List<StudentCourses>> GetListHistoryCourseAsync(string status, string idStudent, int pageSize, int pageNumber)
    {
      if (status == "ALL")
      {
        return await _context.StudentCourses.OrderByDescending(x=>x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
      }
      return await _context.StudentCourses.Where(e => e.status == status && e.idUser == idStudent).OrderByDescending(x=>x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }

    public async Task<StudentCourses> FindByIdCourseAndIdUser(string idCourse, string idUser)
    {
      if (StudentCoursesExistsCourse(idCourse) == true && StudentCoursesExistsUser(idUser) == true)
      {
         var studentCourse = await _context.StudentCourses.Where(e => e.idCourse == idCourse && e.idUser == idUser).FirstOrDefaultAsync();
        return studentCourse;
      }
      else
      {
        return null;
      }
    }

    public async Task<List<StudentCourses>> FindCourseHistory(string status, string idStudent,string search, int pageSize, int pageNumber)
    {
      if (status == "ALL")
      {
        return await _context.StudentCourses.Where(e => e.idUser == idStudent&& e.Course.name.Contains(search)).OrderByDescending(x=>x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
      }
      return await _context.StudentCourses.Where(e => e.status == status && e.idUser == idStudent&& e.Course.name.Contains(search)).OrderByDescending(x=>x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }
  }
}