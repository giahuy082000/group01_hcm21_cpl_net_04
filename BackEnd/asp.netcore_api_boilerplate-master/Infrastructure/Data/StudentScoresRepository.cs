using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class StudentScoresRepository : IStudentScoresRepository
    {
        private readonly AppIdentityDbContext _context;
        public StudentScoresRepository(AppIdentityDbContext context)
        {
            _context = context;
        }

        public async Task<string> CreateStudentScores(StudentScores StudentScores)
        {
            try
            {
                _context.StudentScores.Add(StudentScores);
                var check = await _context.SaveChangesAsync();
                return "Success";
            }
            catch
            {
                return "Failed";
            }
        }

        public async Task<string> UpdateStudentScores(StudentScores StudentScores)
        {
            _context.Entry(StudentScores).State = EntityState.Modified;
            if (!StudentScoresExistsCourse(StudentScores.idCourse))
            {
                return "Course doesn't exist";
            }
            else if (!StudentScoresExistsUser(StudentScores.idUser))
            {
                return "User doesn't exist";
            }
            else if (!StudentScoresExistsAssignment(StudentScores.idAssignment))
            {
                return "Assignment doesn't exist";
            }
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return "Failed";
            }
            return "Success";
        }

        public async Task<StudentScores> FindById(string idCourse, string idUser, string idAssignment)
        {
            if (StudentScoresExistsCourse(idCourse) == true && StudentScoresExistsUser(idUser) == true
            && StudentScoresExistsAssignment(idAssignment) == true)
            {
                var StudentScores = await _context.StudentScores.FindAsync(idCourse, idUser, idAssignment);
                return StudentScores;
            }
            else
            {
                return null;
            }
        }
        private bool StudentScoresExistsCourse(string idCourse)
        {
            return _context.StudentScores.Any(e => e.idCourse == idCourse);
        }
        private bool StudentScoresExistsUser(string idUser)
        {
            return _context.StudentScores.Any(e => e.idUser == idUser);
        }

        private bool StudentScoresExistsAssignment(string idAssignment)
        {
            return _context.StudentScores.Any(e => e.idAssignment == idAssignment);
        }
        public async Task<List<StudentScores>> GetScoreByCourse(string idCourse, int pageSize, int pageNumber)
        {

            return await _context.StudentScores.Where(e => e.idCourse == idCourse).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
        }
        public async Task<List<StudentScores>> GetScoreByStudent(string idStudent, int pageSize, int pageNumber)
        {

            return await _context.StudentScores.Where(e => e.idUser == idStudent).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
        }
        public async Task<List<StudentScores>> GetScoreByAssignment(string idAssignment, int pageSize, int pageNumber)
        {

            return await _context.StudentScores.Where(e => e.idAssignment == idAssignment).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
        }

         public async Task<List<StudentScores>> GetScoreByStudentOfCourse(string idCourse, string idStudent, int pageSize, int pageNumber)
         {
             return await _context.StudentScores.Where(e => e.idCourse == idCourse && e.idUser == idStudent).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
         }

         public async Task<float> GetAVGScoreOfStudent(string idCourse, string idStudent)
         {
             
             float sum = 0;
             var listStudentCourse = await _context.StudentScores.Where(e => e.idCourse == idCourse && e.idUser == idStudent).ToListAsync();
             if (listStudentCourse.Count() == 0)
             {    
                 return sum;
             }
             
             foreach (var studentCourse in listStudentCourse)
             {
                 sum += studentCourse.score;
             }
             float avg = sum/listStudentCourse.Count();
             return avg;
         }

    }
}