using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Dto;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
  public class CreateAssigmentsRepository : ICreateAssigmentsRepository
  {
    private readonly AppIdentityDbContext _context;
    public CreateAssigmentsRepository(AppIdentityDbContext context)
    {
      _context = context;
    }
    public async Task<string> CreateAssigments(CreateAssignments createAssignments)
    {
      try
      {
        _context.CreateAssignments.Add(createAssignments);
        var check = await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    public async Task<string> UpdateAssigments(CreateAssignments createAssignments)
    {
      _context.Entry(createAssignments).State = EntityState.Modified;
      if (!AssigmentExists(createAssignments.Id))
      {
        return "Assigments doesn't exist";
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }
    public async Task<List<CreateAssignments>> GetAllAssignmentsByRoadMapContent(string id)
    {
      return await _context.CreateAssignments.Where(e => e.idRoadMapContent == id).ToListAsync();
    }

    private bool AssigmentExists(string id)
    {
      Console.WriteLine(id);
      return _context.CreateAssignments.Any(e => e.Id == id);
    }

    public async Task<List<CreateAssignments>> FindByName(string searchAssigment)
    {
      return await _context.CreateAssignments.Where(c => c.title.Contains(searchAssigment)).ToListAsync();
    }
    public async Task<CreateAssignments> FindById(string id)
    {
      Console.WriteLine(id);
      if (AssigmentExists(id) == true)
      {
        var assigments = await _context.CreateAssignments.FindAsync(id);
        return assigments;
      }
      else
      {
        return null;
      }
    }

    public async Task<string> DeleteAssigments(string id)
    {
      try
      {
        var assignmments = await _context.CreateAssignments.FindAsync(id);
        if (assignmments == null)
        {
          return null;
        }
        _context.CreateAssignments.Remove(assignmments);
        await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    public async Task<List<DeadLineDto>> GetDeadLine(DateTime startTime, DateTime endTime, String idUser)
    {
      var studentCourses = await _context.StudentCourses.Where(e => e.idUser == idUser && e.status == "STUDYING").ToListAsync();
      var lenghtStudentCourse = studentCourses.Count();
      List<DeadLineDto> arrResult = new List<DeadLineDto>();
      foreach (var itemCourse in studentCourses)
      {
        var roadMaps = await _context.RoadMaps.Where(e => e.idCourse == itemCourse.idCourse).ToListAsync();

        foreach (var itemRoadMap in roadMaps)
        {
          var roadMapContents = await _context.RoadMapContents.Where(e => e.idRoadMap == itemRoadMap.Id).ToListAsync();
          // itemRoadMap.RoadMapContent[0].createAssignment[0];
          foreach (var itemRoadMapContent in roadMapContents)
          {
            var assigments = await _context.CreateAssignments.Where(e => e.idRoadMapContent == itemRoadMapContent.Id && e.start >= startTime && e.finish < endTime).ToListAsync();
            foreach (var itemAssigment in assigments)
            {
              DeadLineDto deadline = new DeadLineDto();
              deadline.idCourse = itemAssigment.RoadMapContent.RoadMap.idCourse;
              var courseCurrent = await _context.Courses.Where(e => e.Id == deadline.idCourse).SingleAsync();
              deadline.idAssigment = itemAssigment.Id;
              deadline.nameAssigment = itemAssigment.title;
              deadline.nameCourse = courseCurrent.name;
              arrResult.Add(deadline);
            }
          }
        }
      }
      return arrResult;
    }

    public async Task<List<BackAssigmentAllDto>> GetAllAssignmentsByIDCourse(string id, int pageNumber, int pageSize)
    {
      var roadMaps = await _context.RoadMaps.Where(e => e.idCourse == id).ToListAsync();
      List<BackAssigmentAllDto> arrResult = new List<BackAssigmentAllDto>();

      foreach (var itemRoadMap in roadMaps)
      {
        var roadMapContents = await _context.RoadMapContents.Where(e => e.idRoadMap == itemRoadMap.Id).ToListAsync();
        // itemRoadMap.RoadMapContent[0].createAssignment[0];
        foreach (var itemRoadMapContent in roadMapContents)
        {
          var assignments = await _context.CreateAssignments.Where(e => e.idRoadMapContent == itemRoadMapContent.Id).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
          foreach (var itemAssigment in assignments)
          {
            BackAssigmentAllDto result = new BackAssigmentAllDto();
            result.idCreator = itemAssigment.idCreator;
            result.idRoadMapContent = itemAssigment.idRoadMapContent;
            result.title = itemAssigment.title;
            result.content = itemAssigment.content;
            result.start = itemAssigment.start;
            result.finish = itemAssigment.finish;
            result.Id = itemAssigment.Id;
            result.urlFile = itemAssigment.urlFile;
            arrResult.Add(result);
          }
        }
      }
      return arrResult;
    }
  }
}

