using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
  public class ForumsRepository : IForumsRepository
  {
    private readonly AppIdentityDbContext _context;
    public ForumsRepository(AppIdentityDbContext context)
    {
      _context = context;
    }

    public async Task<string> CreateForums(Forums Forums)
    {
      try
      {
        _context.Forums.Add(Forums);
        var check = await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    public async Task<string> UpdateForums(Forums Forums)
    {
      _context.Entry(Forums).State = EntityState.Modified;
      if (!ForumsExists(Forums.Id))
      {
        return "Forums doesn't exist";
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }

    public async Task<List<Forums>> GetAllForumsAsync(int pageSize, int pageNumber)
    {
      var status = "ACTIVE";
      return await _context.Forums.Where(e => e.status == status).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }
    public async Task<Forums> FindById(string id)
    {
      if (ForumsExists(id) == true)
      {
        var Forums = await _context.Forums.FindAsync(id);
        return Forums;
      }
      else
      {
        return null;
      }
    }
    private bool ForumsExists(string id)
    {
      return _context.Forums.Any(e => e.Id == id);
    }

    public async Task<List<Forums>> FindByTitle(string searchForums, int pageSize, int pageNumber)
    {
      return await _context.Forums.Where(c => c.title.Contains(searchForums)).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }
  }
}