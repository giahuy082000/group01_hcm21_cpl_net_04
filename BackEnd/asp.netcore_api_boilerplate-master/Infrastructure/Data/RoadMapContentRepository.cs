using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
  public class RoadMapContentRepository : IRoadmapContentRepository
  {
    private readonly AppIdentityDbContext _context;
    public RoadMapContentRepository(AppIdentityDbContext context)
    {
      _context = context;
    }

    public async Task<string> CreateRoadMapContent(RoadMapContents RoadMapContents)
    {
      try
      {
        _context.RoadMapContents.Add(RoadMapContents);
        var check = await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    public async Task<string> UpdateRoadMapContent(RoadMapContents RoadMapContents)
    {
      _context.Entry(RoadMapContents).State = EntityState.Modified;
      if (!RoadMapContentExists(RoadMapContents.Id))
      {
        return "RoadMapContents doesn't exist";
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }

        public async Task<List<RoadMapContents>> GetAllRoadMapContentsOfRM(string idRoadMap, int pageSize, int pageNumber)
        {
            return await _context.RoadMapContents.Where(e => e.idRoadMap == idRoadMap).OrderBy(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
        }
        public async Task<RoadMapContents> FindById(string id)
        {
            if (RoadMapContentExists(id) == true)
            {
                var RoadMapContents = await _context.RoadMapContents.FindAsync(id);
                return RoadMapContents;
            }
            else
            {
                return null;
            }
        }

    public async Task<string> DeleteRoadMapContent(string id)
    {
      var RoadMapContents = await _context.RoadMapContents.FindAsync(id);

      if (!RoadMapContentExists(RoadMapContents.Id))
      {
        return "RoadMapContents doesn't exist";
      }
      try
      {
        _context.RoadMapContents.Remove(RoadMapContents);
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }
    private bool RoadMapContentExists(string id)
    {
      return _context.RoadMapContents.Any(e => e.Id == id);
    }


  }
}