using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
  public class EveluateCoursesRepository : IEveluateCourseRepository
  {
    private readonly AppIdentityDbContext _context;
    public EveluateCoursesRepository(AppIdentityDbContext context)
    {
      _context = context;
    }


    public async Task<List<EveluateCourses>> GetAllEveluateCoursesAsync(string id)
    {
      return await _context.EveluateCourses.Where(e=> e.idCourse == id).ToListAsync();
    }

    public async Task<string> CreateEveluate(EveluateCourses eveluateCourses)
    {
      try
      {
        _context.EveluateCourses.Add(eveluateCourses);
        var check = await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }
    
    public async Task<string> FindByUserAndCourseAsync(string idUser, string idCourse)
    {
      try
      {
        Console.WriteLine("idUser");
        Console.WriteLine(idUser);
        Console.WriteLine(idCourse);
        var eveluate =  await _context.EveluateCourses.Where(e => e.idCourse == idCourse && e.idUser == idUser).ToListAsync();
        Console.WriteLine("eveluate count");
        Console.WriteLine(eveluate.Count());
        if(eveluate.Count() != 0)
        {
          return "Success";
        }
        return "Failed";
      }
      catch
      {
        return "Failed";
      }

    }

    public async Task<string> UpdateEveluate(EveluateCourses eveluateCourses)
    {
      _context.Entry(eveluateCourses).State = EntityState.Modified;
      if (!EveluateCoursesExists(eveluateCourses.Id))
      {
        return "Eveluate doesn't exist";
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }

    public async Task<string> DeleteEveluate(string id)
    {
      try
      {
        var eveluate = await _context.EveluateCourses.FindAsync(id);
        if (eveluate == null)
        {
          return null;
        }
        _context.EveluateCourses.Remove(eveluate);
        await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    async Task<EveluateCourses> IEveluateCourseRepository.FindById(string id)
    {
      if (EveluateCoursesExists(id) == true)
      {
        var eveluate = await _context.EveluateCourses.FindAsync(id);
        return eveluate;
      }
      else
      {
        return null;
      }
    }
    private bool EveluateCoursesExists(string id)
    {
      return _context.EveluateCourses.Any(e => e.Id == id);
    }

    // public async Task<List<Courses>> FindByName(string searchCourse)
    // {
    //   var status = "ACTIVE";
    //   return await _context.Courses.Where(c => c.name.Contains(searchCourse) && c.status==status ).ToListAsync();
    // }
  }
}