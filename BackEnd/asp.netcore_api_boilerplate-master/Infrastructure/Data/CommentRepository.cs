using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
  public class CommentRepository : ICommentRepository
  {
    private readonly AppIdentityDbContext _context;
    public CommentRepository(AppIdentityDbContext context)
    {
      _context = context;
    }
    public async Task<List<Comments>> GetAllCommentAsync(string idForum)
    {
      Console.WriteLine(idForum);
      return await _context.Comments.Where(e => e.idForum == idForum && e.status == "ACTIVE").ToListAsync();
    }
    public async Task<string> CreateComment(Comments comments)
    {
      try
      {
        _context.Comments.Add(comments);
        var check = await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    public async Task<string> UpdateComment(Comments comments)
    {
      _context.Entry(comments).State = EntityState.Modified;
      if (!CommentExists(comments.Id))
      {
        return "Comment doesn't exist";
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }

    public async Task<Comments> FindById(string id)
    {
      if (CommentExists(id) == true)
      {
        var comment = await _context.Comments.FindAsync(id);
        return comment;
      }
      else
      {
        return null;
      }
    }

    private bool CommentExists(string id)
    {
      return _context.Comments.Any(e => e.Id == id);
    }

  }
}

