using Core.Entities;
using Core.Entities.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Identity
{
    public class AppIdentityDbContext : IdentityDbContext<AppUser>
    {
        public DbSet<Comments> Comments { get; set; }
        public DbSet<CreateAssignments> CreateAssignments { get; set; }
        public DbSet<Courses> Courses{get;set;}
        public DbSet<Forums> Forums{get;set;}
        public DbSet<RoadMaps> RoadMaps{get;set;}
        public DbSet<RoadMapContents> RoadMapContents{get;set;}
        public DbSet<StudentCourses> StudentCourses{get;set;}
        public DbSet<StudentScores> StudentScores{get;set;}
        public DbSet<Documents> Documents{get;set;}
        public DbSet<MentorCourses> MentorCourses{get;set;}
        public DbSet<EveluateCourses> EveluateCourses{get;set;}


        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> options) : base(options)
        {
        }
    protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<StudentScores>(entity =>
            {
                // Tạo Index Unique trên 2 cột
                entity.HasIndex(p => new { p.idCourse, p.idUser ,p.idAssignment})
                            .IsUnique();
                entity.HasKey(p =>new {
                    p.idCourse,p.idUser,p.idAssignment
                });

            });
            builder.Entity<StudentCourses>(entity =>
            {
                // Tạo Index Unique trên 2 cột
                entity.HasIndex(p => new { p.idCourse, p.idUser })
                            .IsUnique();
                entity.HasKey(p =>new {
                    p.idCourse,p.idUser
                });

            });
            builder.Entity<RoadMapContents>()
            .HasOne(i => i.RoadMap)
            .WithMany(c => c.RoadMapContent)
            .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<CreateAssignments>()
            .HasOne(i => i.RoadMapContent)
            .WithMany(c => c.createAssignment)
            .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Documents>()
            .HasOne(i => i.RoadMapContent)
            .WithMany(c => c.documents)
            .OnDelete(DeleteBehavior.Cascade);
            base.OnModelCreating(builder);
            foreach (var entityType in builder.Model.GetEntityTypes ()) {
                var tableName = entityType.GetTableName ();
                if (tableName.StartsWith ("AspNet")) {
                    entityType.SetTableName (tableName.Substring (6));
                }
            }
        }
         private ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                    builder.AddConsole()
                           .AddFilter(DbLoggerCategory.Database.Command.Name,
                                    LogLevel.Information));
            return serviceCollection.BuildServiceProvider()
                    .GetService<ILoggerFactory>();
        }
    }
}