using AutoMapper;
using Core.Dto;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Infrastructure.Services
{
    public class MapService : Profile
    {
        public MapService()
        {
            CreateMap<AppUser, UserBackDto>();
            CreateMap<Courses,CourseBackDto>();
            CreateMap<StudentCourses, StudentCoursesAVGDto>();
            CreateMap<CreateAssignments, CreateAssigmentsBackDto>();
            CreateMap<RoadMapContents, ReturnRoadMapContentDto>();
        }
    }
}