using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Core.Dto;
using Core.Entities.Identity;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Infrastructure.Services
{
    public class UploadService : IUploadService
    {
        private readonly IConfiguration _config;
        public UploadService(IConfiguration config)
        {
            _config = config;
        }

        public GetLinkDto GetLinkAssigment(string name)
        {
            try
            {
                var bucketName = _config["AwsSettings:BUCKET"];
                var credentials = new BasicAWSCredentials(_config["AwsSettings:AWS_ACCESS_KEY"], _config["AwsSettings:AWS_SECRET_KEY"]);
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.APSoutheast1
                };
                using var client = new AmazonS3Client(credentials, config);
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
                {
                    BucketName = bucketName,
                    Key = name,
                    Expires = DateTime.Now.AddDays(40)
                };
                string path = client.GetPreSignedURL(request);
                var result = new GetLinkDto()
                {
                    linkUrl = path
                };
                return result;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null
                    && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public GetLinkDto GetLinkAvatar(string name)
        {
            try
            {
                var bucketName = _config["AwsSettings:BUCKET"];
                var credentials = new BasicAWSCredentials(_config["AwsSettings:AWS_ACCESS_KEY"], _config["AwsSettings:AWS_SECRET_KEY"]);
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.APSoutheast1
                };
                using var client = new AmazonS3Client(credentials, config);
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
                {
                    BucketName = bucketName,
                    Key = name,
                    Expires = DateTime.Now.AddYears(10)
                };
                string path = client.GetPreSignedURL(request);
                var result = new GetLinkDto()
                {
                    linkUrl = path
                };
                return result;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null
                    && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public GetLinkDto GetLinkDocument(string name)
        {
            try
            {
                var bucketName = _config["AwsSettings:BUCKET"];
                var credentials = new BasicAWSCredentials(_config["AwsSettings:AWS_ACCESS_KEY"], _config["AwsSettings:AWS_SECRET_KEY"]);
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.APSoutheast1
                };
                using var client = new AmazonS3Client(credentials, config);
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
                {
                    BucketName = bucketName,
                    Key = name,
                    Expires = DateTime.Now.AddDays(40)
                };
                string path = client.GetPreSignedURL(request);
                var result = new GetLinkDto()
                {
                    linkUrl = path
                };
                return result;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null
                    && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public GetLinkDto GetLinkImage(string name)
        {
            try
            {
                var bucketName = _config["AwsSettings:BUCKET"];
                var credentials = new BasicAWSCredentials(_config["AwsSettings:AWS_ACCESS_KEY"], _config["AwsSettings:AWS_SECRET_KEY"]);
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.APSoutheast1
                };
                using var client = new AmazonS3Client(credentials, config);
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
                {
                    BucketName = bucketName,
                    Key = name,
                    Expires = DateTime.Now.AddDays(30)
                };
                string path = client.GetPreSignedURL(request);
                var result = new GetLinkDto()
                {
                    linkUrl = path
                };
                return result;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null
                    && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<UploadDto> UploadAssigment(string id, IFormFile file)
        {
            try
            {
                Console.WriteLine(file);
                var bucketName = _config["AwsSettings:BUCKET"];
                var credentials = new BasicAWSCredentials(_config["AwsSettings:AWS_ACCESS_KEY"], _config["AwsSettings:AWS_SECRET_KEY"]);
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.APSoutheast1
                };

                using var client = new AmazonS3Client(credentials, config);
                await using var newMemoryStream = new MemoryStream();
                await file.CopyToAsync(newMemoryStream);

                // var fileExtension = Path.GetExtension(file.FileName);
                Console.WriteLine(file.FileName);
                Console.WriteLine(file.ContentType);
                string strNameAvatar = "Assigments/" + id + file.FileName;
                // URL for Accessing Document for Demo
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = newMemoryStream,
                    BucketName = bucketName,
                    Key = strNameAvatar,
                    ContentType = file.ContentType,
                    //   CannedACL = S3CannedACL.PublicRead,
                };

                var fileTransferUtility = new TransferUtility(client);
                await fileTransferUtility.UploadAsync(uploadRequest);
                //Get link URL
                var path = GetLinkAssigment(strNameAvatar);
                var uploadFile = new UploadDto
                {
                    nameFile = strNameAvatar,
                    url = path.linkUrl
                };
                return uploadFile;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null
                    && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<UploadDto> UploadAvatar(string id, IFormFile file)
        {
            try
            {
                Console.WriteLine(file);
                var bucketName = "fpt-intern";

                var credentials = new BasicAWSCredentials(_config["AwsSettings:AWS_ACCESS_KEY"], _config["AwsSettings:AWS_SECRET_KEY"]);
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.APSoutheast1
                };

                using var client = new AmazonS3Client(credentials, config);
                await using var newMemoryStream = new MemoryStream();
                await file.CopyToAsync(newMemoryStream);

                // var fileExtension = Path.GetExtension(file.FileName);
                Console.WriteLine(file.FileName);
                Console.WriteLine(file.ContentType);
                string strNameAvatar = "Avatar/" + id + file.FileName;
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = newMemoryStream,
                    BucketName = bucketName,
                    Key = strNameAvatar,
                    ContentType = file.ContentType,
                    //   CannedACL = S3CannedACL.PublicRead,
                };
                var fileTransferUtility = new TransferUtility(client);
                await fileTransferUtility.UploadAsync(uploadRequest);
                var path = GetLinkAvatar(strNameAvatar);
                var uploadFile = new UploadDto
                {
                    nameFile = strNameAvatar,
                    url = path.linkUrl
                };
                return uploadFile;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null
                    && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }

        }
        public async Task<UploadDto> UploadDocument(string id, IFormFile file)
        {
            try
            {
                Console.WriteLine(file);
                var bucketName = _config["AwsSettings:BUCKET"];

                var credentials = new BasicAWSCredentials(_config["AwsSettings:AWS_ACCESS_KEY"], _config["AwsSettings:AWS_SECRET_KEY"]);
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.APSoutheast1
                };

                using var client = new AmazonS3Client(credentials, config);
                await using var newMemoryStream = new MemoryStream();
                await file.CopyToAsync(newMemoryStream);

                // var fileExtension = Path.GetExtension(file.FileName);
                Console.WriteLine(file.FileName);
                Console.WriteLine(file.ContentType);
                string strNameDocument = "Documents/" + id + file.FileName;
                // URL for Accessing Document for Demo
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = newMemoryStream,
                    BucketName = bucketName,
                    Key = strNameDocument,
                    ContentType = file.ContentType,
                    //   CannedACL = S3CannedACL.PublicRead,
                };

                var fileTransferUtility = new TransferUtility(client);
                await fileTransferUtility.UploadAsync(uploadRequest);
                //Get link URL
                var path = GetLinkDocument(strNameDocument);
                var uploadFile = new UploadDto
                {
                    nameFile = strNameDocument,
                    url = path.linkUrl
                };
                return uploadFile;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null
                    && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<UploadDto> UploadSubmit(string idStudent, string idAssigment, IFormFile file)
        {
            try
            {
                Console.WriteLine(file);
                var bucketName = _config["AwsSettings:BUCKET"];

                var credentials = new BasicAWSCredentials(_config["AwsSettings:AWS_ACCESS_KEY"], _config["AwsSettings:AWS_SECRET_KEY"]);
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.APSoutheast1
                };

                using var client = new AmazonS3Client(credentials, config);
                await using var newMemoryStream = new MemoryStream();
                await file.CopyToAsync(newMemoryStream);

                // var fileExtension = Path.GetExtension(file.FileName);
                Console.WriteLine(file.FileName);
                Console.WriteLine(file.ContentType);
                string strNameDocument = "Submits/" + idAssigment + file.FileName;
                // URL for Accessing Document for Demo
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = newMemoryStream,
                    BucketName = bucketName,
                    Key = strNameDocument,
                    ContentType = file.ContentType,
                    //   CannedACL = S3CannedACL.PublicRead,
                };

                var fileTransferUtility = new TransferUtility(client);
                await fileTransferUtility.UploadAsync(uploadRequest);
                //Get link URL
                var path = GetLinkDocument(strNameDocument);
                var uploadFile = new UploadDto
                {
                    nameFile = strNameDocument,
                    url = path.linkUrl
                };
                return uploadFile;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null
                    && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }
        public async Task<UploadDto> UploadImage(string id, IFormFile file)
        {
            try
            {
                Console.WriteLine(file);
                var bucketName = "fpt-intern";

                var credentials = new BasicAWSCredentials(_config["AwsSettings:AWS_ACCESS_KEY"], _config["AwsSettings:AWS_SECRET_KEY"]);
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.APSoutheast1
                };

                using var client = new AmazonS3Client(credentials, config);
                await using var newMemoryStream = new MemoryStream();
                await file.CopyToAsync(newMemoryStream);

                // var fileExtension = Path.GetExtension(file.FileName);
                Console.WriteLine(file.FileName);
                Console.WriteLine(file.ContentType);
                string strNameImage = "Image/" + id + file.FileName;
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = newMemoryStream,
                    BucketName = bucketName,
                    Key = strNameImage,
                    ContentType = file.ContentType,
                    //   CannedACL = S3CannedACL.PublicRead,
                };
                var fileTransferUtility = new TransferUtility(client);
                await fileTransferUtility.UploadAsync(uploadRequest);
                var path = GetLinkImage(strNameImage);
                var uploadFile = new UploadDto
                {
                    nameFile = strNameImage,
                    url = path.linkUrl
                };
                return uploadFile;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null
                    && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }
        public async Task<UploadDto> UploadImageCourse(string id, IFormFile file)
        {
            try
            {
                Console.WriteLine(file);
                var bucketName = "fpt-intern";

                var credentials = new BasicAWSCredentials(_config["AwsSettings:AWS_ACCESS_KEY"], _config["AwsSettings:AWS_SECRET_KEY"]);
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.APSoutheast1
                };

                using var client = new AmazonS3Client(credentials, config);
                await using var newMemoryStream = new MemoryStream();
                await file.CopyToAsync(newMemoryStream);

                // var fileExtension = Path.GetExtension(file.FileName);
                Console.WriteLine(file.FileName);
                Console.WriteLine(file.ContentType);
                string strNameImage = "ImageCourse/" + id + file.FileName;
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = newMemoryStream,
                    BucketName = bucketName,
                    Key = strNameImage,
                    ContentType = file.ContentType,
                    //   CannedACL = S3CannedACL.PublicRead,
                };
                var fileTransferUtility = new TransferUtility(client);
                await fileTransferUtility.UploadAsync(uploadRequest);
                var path = GetLinkAvatar(strNameImage);
                var uploadFile = new UploadDto
                {
                    nameFile = strNameImage,
                    url = path.linkUrl
                };
                return uploadFile;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null
                    && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }

    }
}